<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_pelanggan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('kyc_id')->nullable();
            $table->unsignedBigInteger('perusahaan_id')->nullable();
            $table->string('username', 100)->nullable();
            $table->string('password', 50)->nullable();
            $table->string('nama_pelanggan', 100)->nullable();
            $table->string('tempat_lahir', 100)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->char('gender', 1)->nullable();
            $table->string('alamat_ktp', 500)->nullable();
            $table->string('alamat_domisili', 500)->nullable();
            $table->string('kota_domisili', 50)->nullable();
            $table->string('provinsi_domisili', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('no_hp', 100)->nullable();
            $table->string('fb', 100)->nullable();
            $table->string('profile_photo', 100)->nullable();
            $table->json('kyc')->nullable();
            $table->string('jabatan', 20)->nullable();
            $table->integer('skor_kuesioner')->nullable();
            $table->string('status_pelanggan', 20)->nullable();
            $table->string('status_akun', 20)->nullable();
            $table->string('role_pelanggan', 20)->nullable();
            $table->string('status_mitra', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_pelanggan');
    }
};

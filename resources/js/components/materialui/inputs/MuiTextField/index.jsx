import React from 'react';
import PropTypes from 'prop-types';

import Mockup from './Mockup';

const MuiTextField = ({ id, setInput, ...props }) => {
    return (
        <Mockup
            id={id}
            onChange={(e) => {
                setInput((prevState) => ({
                    ...prevState,
                    [id]: {
                        ...prevState[id],
                        value: e.target.value,
                        invalid: '',
                    },
                }));
            }}
            {...props}
        />
    );
};

MuiTextField.defaultProps = {
    value: '',
    valueType: 'text',
    placeholder: '',
    labelRequired: true,
    invalid: '',
    theme: 'dark',
};

MuiTextField.propTypes = {
    id: PropTypes.string.isRequired,
    setInput: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    valueType: PropTypes.oneOf(['text', 'password']),
    labelRequired: PropTypes.bool,
    InputProps: PropTypes.object,
    invalid: PropTypes.string,
    theme: PropTypes.oneOf(['white', 'dark']),
};

export default MuiTextField;

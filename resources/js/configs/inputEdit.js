import { typeIsNull, typeIsUndefined } from '@hooks/type';

const inputEdit = (prevState, { data = {}, ...result }) => {
    let newState = {};
    for (let [key, val] of Object.entries(prevState)) {
        if (val.type === 'select') {
            newState[key] = {
                ...val,
                options: result[val.optionsValue] ?? result[key] ?? val.optionsDefault ?? [],
                value:
                    !typeIsUndefined(data[val.valueEdit]) && !typeIsNull(data[val.valueEdit])
                        ? data[val.valueEdit].toString()
                        : !typeIsUndefined(data[key]) && !typeIsNull(data[key])
                        ? data[key].toString()
                        : '',
                invalid: '',
            };
        } else {
            newState[key] = {
                ...val,
                value:
                    !typeIsUndefined(data[val.valueEdit]) && !typeIsNull(data[val.valueEdit])
                        ? data[val.valueEdit].toString()
                        : !typeIsUndefined(data[key]) && !typeIsNull(data[key])
                        ? data[key].toString()
                        : '',
                invalid: '',
            };
        }
    }
    return newState;
};

export default inputEdit;

import i18next from 'i18next';

import { typeIsUndefined } from '@hooks/type';

import notistack from './notistack';

const apiCatch = (error, setInput, setError) => {
    if (error.response) {
        // The client was given an error response (5xx, 4xx)
        const { errors, message, type = 'error' } = error.response.data;
        const status = error.response.status;
        const statusText = error.response.statusText;

        if (setInput && status === 422) {
            setInput((prevState) => {
                let input = {};

                for (const [key, val] of Object.entries(prevState)) {
                    input[key] =
                        typeIsUndefined(errors[key]) === true
                            ? val
                            : {
                                  ...prevState[key],
                                  invalid: errors[key].length < 1 ? '' : errors[key][0],
                              };
                }

                return input;
            });
        } else {
            if (setError) {
                setError(i18next.t(message.length < 1 ? statusText : message));
            } else {
                // eslint-disable-next-line prettier/prettier
                if (['success', 'warning', 'info', 'error', 'default'].some(err => err === 'type')) {
                    notistack[type](
                        i18next.t(
                            typeIsUndefined(message) || message.length < 1 ? statusText : message
                        )
                    );
                } else {
                    notistack['error'](
                        i18next.t(
                            typeIsUndefined(message) || message.length < 1 ? statusText : message
                        )
                    );
                }
            }
        }
    } else if (error.request) {
        notistack['error']('Belum di setting request.');
    } else {
        notistack['error'](i18next.t(error.toString()));
    }
};

export default apiCatch;

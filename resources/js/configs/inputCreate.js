import { typeIsNull, typeIsUndefined } from '@hooks/type';

const inputCreate = (prevState, result) => {
    let newState = {};
    for (let [key, val] of Object.entries(prevState)) {
        if (val.type === 'select') {
            newState[key] = {
                ...val,
                options: result[val.optionsValue] ?? result[key] ?? val.optionsDefault ?? [],
                value:
                    !typeIsUndefined(val.valueDefault) && !typeIsNull(val.valueDefault)
                        ? val.valueDefault?.toString()
                        : '',
                invalid: '',
            };
        } else {
            newState[key] = {
                ...val,
                value:
                    !typeIsUndefined(val.valueDefault) && !typeIsNull(val.valueDefault)
                        ? val.valueDefault?.toString()
                        : '',
                invalid: '',
            };
        }
    }
    return newState;
};

export default inputCreate;

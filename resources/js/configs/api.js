import Axios from 'axios';

const api = Axios.create({
    baseURL: window.location.origin + '/api',
    headers: {
        'Content-type': 'application/json',
        'Content-Type': 'multipart/form-data',
        common: {
            'X-Requested-With': 'XMLHttpRequest',
        },
    },
});

export default api;

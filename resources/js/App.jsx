import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import { Backdrop, CircularProgress, CssBaseline, Typography } from '@mui/material';

import { VerticalLayout } from './layouts';

// eslint-disable-next-line prettier/prettier
const App = ({ app_backdrop_open, app_backdrop_progress, app_backdrop_content }) => {
    return (
        <>
            <CssBaseline />

            {app_backdrop_open === true && (
                <Backdrop
                    sx={{
                        color: '#fff',
                        flexDirection: 'column',
                        zIndex: (theme) => theme.zIndex.drawer + 1,
                    }}
                    open={app_backdrop_open}>
                    {app_backdrop_progress.length < 1 && <CircularProgress color="inherit" />}
                    <Typography
                        variant="h4"
                        gutterBottom
                        component="div"
                        sx={{ marginBottom: 0 }}
                        children={app_backdrop_content}
                    />
                </Backdrop>
            )}

            <BrowserRouter>
                <Routes>
                    <Route
                        path="/"
                        element={<Navigate to="/pelanggan-daftar-pelanggan" replace={true} />}
                    />
                    <Route path="*" element={<VerticalLayout />} />
                </Routes>
            </BrowserRouter>
        </>
    );
};

const mapStateToProps = ({ app }) => {
    return {
        app_backdrop_open: app.app_backdrop_open,
        app_backdrop_content: app.app_backdrop_content,
        app_backdrop_progress: app.app_backdrop_progress,
    };
};

App.propTypes = {
    app_backdrop_open: PropTypes.bool.isRequired,
    app_backdrop_content: PropTypes.string.isRequired,
    app_backdrop_progress: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, null)(App);

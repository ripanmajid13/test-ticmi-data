import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Box, Collapse, Divider, Grid, Paper, Skeleton } from '@mui/material';

import { typeIsFunction } from '@hooks/type';
import Page404 from '@views/errors/Error404';

const LoadingTable = ({ children, result, setData }) => {
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (result?.status) {
            if (result.status === 200) {
                if (setData && typeIsFunction(setData)) {
                    setData(result.data);
                }
            } else {
                setError(true);
            }

            setLoading(false);
        }
    }, [result]);

    if (loading) {
        return (
            <Paper
                elevation={0}
                sx={{
                    p: 1,
                    boxShadow:
                        'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px',
                }}>
                <Skeleton
                    animation="wave"
                    variant="rounded"
                    width={130}
                    height={28}
                    sx={{ mb: 2 }}
                />

                <Grid container spacing={1}>
                    <Grid item xs={12} sm={3} sx={{ pt: '0 !important', mb: 1 }}>
                        <Skeleton
                            animation="wave"
                            variant="rounded"
                            width={100}
                            height={22}
                            sx={{ marginBottom: '2px' }}
                        />
                        <Skeleton animation="wave" variant="rounded" height={31} />
                    </Grid>

                    <Grid item xs={12} sm={3} sx={{ pt: '0 !important', mb: 1 }}>
                        <Skeleton
                            animation="wave"
                            variant="rounded"
                            width={100}
                            height={22}
                            sx={{ marginBottom: '2px' }}
                        />
                        <Skeleton animation="wave" variant="rounded" height={31} />
                    </Grid>

                    <Grid item xs={12} sm={3} sx={{ pt: '0 !important', mb: 1 }}>
                        <Skeleton
                            animation="wave"
                            variant="rounded"
                            width={100}
                            height={22}
                            sx={{ marginBottom: '2px' }}
                        />
                        <Skeleton animation="wave" variant="rounded" height={31} />
                    </Grid>

                    <Grid item xs={12} sm={3} sx={{ pt: '0 !important', mb: 1 }}>
                        <Skeleton
                            animation="wave"
                            variant="rounded"
                            width={100}
                            height={22}
                            sx={{ marginBottom: '2px' }}
                        />
                        <Skeleton animation="wave" variant="rounded" height={31} />
                    </Grid>
                </Grid>

                <Divider sx={{ mb: 1, backgroundColor: 'rgba(224, 224, 224, 1)' }} />

                <Box sx={{ display: 'flex' }}>
                    <Skeleton
                        animation="wave"
                        variant="rounded"
                        width={100}
                        height={31}
                        sx={{ marginBottom: '8px' }}
                    />{' '}
                    &nbsp;&nbsp;
                    <Skeleton
                        animation="wave"
                        variant="rounded"
                        width={100}
                        height={31}
                        sx={{ marginBottom: '8px' }}
                    />
                </Box>

                <Skeleton animation="wave" variant="rectangular" width="100%" height={300} />

                <Box sx={{ mt: 1, display: 'flex', justifyContent: 'space-between' }}>
                    <Box>
                        <Box sx={{ display: 'flex' }}>
                            <Skeleton animation="wave" variant="rounded" width={200} height={31} />
                        </Box>
                    </Box>

                    <Box>
                        <Box sx={{ display: 'flex' }}>
                            <Skeleton
                                animation="wave"
                                variant="rounded"
                                width={40}
                                height={31}
                                sx={{ mr: 1 }}
                            />
                            <Skeleton
                                animation="wave"
                                variant="rounded"
                                width={40}
                                height={31}
                                sx={{ mr: 1 }}
                            />
                            <Skeleton
                                animation="wave"
                                variant="rounded"
                                width={40}
                                height={31}
                                sx={{ mr: 1 }}
                            />
                            <Skeleton
                                animation="wave"
                                variant="rounded"
                                width={40}
                                height={31}
                                sx={{ mr: 1 }}
                            />
                            <Skeleton
                                animation="wave"
                                variant="rounded"
                                width={40}
                                height={31}
                                sx={{ mr: 1 }}
                            />
                        </Box>
                    </Box>
                </Box>
            </Paper>
        );
    } else {
        if (error) {
            return <Page404 />;
        } else {
            if (result?.data) {
                return (
                    <Collapse
                        in={error === false && loading === false}
                        style={{ transformOrigin: '0 0 0' }}
                        timeout={1000}
                        unmountOnExit>
                        <Paper
                            elevation={0}
                            sx={{
                                p: 1,
                                boxShadow:
                                    'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px',
                            }}>
                            {children}
                        </Paper>
                    </Collapse>
                );
            } else {
                return <Page404 message="Data must be an object of HelperDataTable" />;
            }
        }
    }
};

LoadingTable.propTypes = {
    children: PropTypes.node,
    result: PropTypes.object.isRequired,
    setData: PropTypes.func,
};

export default LoadingTable;

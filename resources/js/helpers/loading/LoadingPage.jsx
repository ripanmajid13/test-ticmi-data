import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { CircularProgress, Collapse, Paper, Typography } from '@mui/material';

import { typeIsFunction } from '@hooks/type';
import Page404 from '@views/errors/Error404';

const LoadingPage = ({ children, result, setData }) => {
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (result?.status) {
            setTimeout(() => {
                if (result.status === 200) {
                    if (setData && typeIsFunction(setData)) {
                        setData(result.data);
                    }
                } else {
                    setError(true);
                }

                setLoading(false);
            }, [300]);
        }
    }, [result]);

    if (loading) {
        return (
            <Paper
                elevation={0}
                sx={{
                    py: 4,
                    // eslint-disable-next-line prettier/prettier
                    boxShadow: 'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px',
                }}>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        position: 'relative',
                    }}>
                    <CircularProgress disableShrink size={30} /> &nbsp;&nbsp;{' '}
                    <Typography variant="h5" component="h6" children={'Loading ...'} />
                </div>
            </Paper>
        );
    } else {
        if (error) {
            return (
                <Page404
                    status={result.status || '404'}
                    message={result.message || result.statusText}
                />
            );
        } else {
            return (
                <Collapse
                    in={error === false && loading === false}
                    style={{ transformOrigin: '0 0 0' }}
                    timeout={1000}
                    unmountOnExit>
                    {children}
                </Collapse>
            );
        }
    }
};

LoadingPage.propTypes = {
    children: PropTypes.node,
    result: PropTypes.object.isRequired,
    setData: PropTypes.func,
};

export default LoadingPage;

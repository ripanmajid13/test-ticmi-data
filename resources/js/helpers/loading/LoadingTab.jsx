import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Box, CircularProgress, Collapse, Typography} from '@mui/material';

const LoadingTab = ({children, result, setData}) => {
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (result?.status) {
            if (result.status === 200) {
                if (setData) {
                    setData(result.data);
                }
            } else {
                setError(true);
            }

            setLoading(false);
        }
    }, [result]);

    if (loading) {
        return (
            <Box className="p-2" elevation={0} sx={{mb: '1rem'}}>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        position: 'relative',
                        height: 'calc(100vh - (16.2rem + 1px))',
                    }}>
                    <CircularProgress disableShrink size={30} /> &nbsp;&nbsp;{' '}
                    <Typography
                        variant="h5"
                        component="h6"
                        children={'Loading ...'}
                    />
                </div>
            </Box>
        );
    } else {
        if (error) {
            return (
                <Box className="p-2" elevation={0} sx={{mb: '1rem'}}>
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                            position: 'relative',
                            height: 'calc(100vh - (13.1rem + 1px))',
                        }}>
                        <Typography variant="h1" style={{color: 'red'}}>
                            {result.status || '404'}
                        </Typography>

                        <Typography variant="h6" style={{color: 'red'}}>
                            {result.message ||
                                result.statusText ||
                                'The page you’re looking for doesn’t exist.'}
                        </Typography>
                    </div>
                </Box>
            );
        } else {
            return (
                <Collapse
                    in={true}
                    style={{transformOrigin: '0 0 0'}}
                    timeout={1000}
                    unmountOnExit>
                    <Box className="p-2" elevation={0}>
                        {children}
                    </Box>
                </Collapse>
            );
        }
    }
};

LoadingTab.propTypes = {
    children: PropTypes.node,
    result: PropTypes.object.isRequired,
    setData: PropTypes.func,
};

export default LoadingTab;

import LoadingPage from './LoadingPage';
import LoadingTab from './LoadingTab';
import LoadingTable from './LoadingTable';

export { LoadingPage, LoadingTab, LoadingTable };

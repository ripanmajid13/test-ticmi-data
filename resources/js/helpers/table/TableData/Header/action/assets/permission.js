import { matchPath } from 'react-router-dom';

import xsrfToken from '@configs/xsrfToken';

const permission = (url) => {
    return true;
};

export default permission;

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useLocation, useNavigate } from 'react-router-dom';
import i18next from 'i18next';

import { ArrowBack as ArrowBackIcon, Save as SaveIcon } from '@mui/icons-material';
import { Box, Checkbox, Divider, FormControlLabel, Paper, Skeleton } from '@mui/material';
import { MuiButton } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import inputCreate from '@configs/inputCreate';
import inputEdit from '@configs/inputEdit';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { formValidation } from '@hooks/form';
import { typeIsUndefined } from '@hooks/type';
import Error404 from '@views/errors/Error404';

import Mockup from './Mockup';

const FormPageLoading = ({ home, input, setInput, type, method, action, payload, repeat }) => {
    let mockup = [];
    let navigate = useNavigate();
    let location = useLocation();

    const [result, setResult] = useState({});
    const [submit, setSubmit] = useState(false);
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        let mounted = true;
        const postData = async () => {
            try {
                let headers = {};
                const formData = new FormData();

                if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                    headers['Authorization'] = xsrfToken().token;
                }

                formData.append('_method', 'POST');
                for (const [key, value] of Object.entries(payload)) {
                    formData.append(key, value);
                }
                await api
                    .post(location.pathname, formData, { headers: headers })
                    .then((response) => {
                        if (mounted) {
                            if (type === 'edit') {
                                setInput((prevState) => inputEdit(prevState, response.data ?? {}));
                            } else {
                                // eslint-disable-next-line prettier/prettier
                                setInput((prevState) => inputCreate(prevState, response.data ?? {}));
                            }

                            setResult({
                                data: response.data,
                                status: response.status,
                                statusText: response.statusText,
                                headers: response.headers,
                            });
                        }
                    })
                    .catch(
                        ({ response }) =>
                            mounted &&
                            setResult({
                                status: response.status,
                                message: response.data.message ?? response.statusText,
                            })
                    );
            } catch ({ response }) {
                mounted &&
                    setResult({
                        status: response.status,
                        message: response.data.message ?? response.statusText,
                    });
            }
        };
        setTimeout(() => {
            postData();
        }, 1000);
        return () => (mounted = false);
    }, []);

    const handleOnSubmit = (e) => {
        e.preventDefault();

        formValidation(input, setInput).then((resForm) => {
            setSubmit(true);
            const postData = async () => {
                try {
                    const formData = new FormData();

                    formData.append('_method', method);
                    for (const [key, value] of Object.entries(resForm)) {
                        formData.append(key, value);
                    }

                    await api
                        .post(home + action, formData, {
                            headers: {
                                ...(Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token') && {
                                    Authorization: xsrfToken().token,
                                }),
                            },
                        })
                        .then(({ data, statusText }) => {
                            setSubmit(false);
                            notistack['success'](i18next.t(data.message || statusText));

                            if (repeat) {
                                if (checked) {
                                    setInput((prevState) => {
                                        let newState = {};
                                        for (let [key, val] of Object.entries(prevState)) {
                                            if (val.type === 'select') {
                                                newState[key] = {
                                                    ...val,
                                                    value: val.valueDefault
                                                        ? val.valueDefault.toString()
                                                        : '',
                                                    options: data.data
                                                        ? data.data[key]
                                                        : val.options ?? [],
                                                    invalid: '',
                                                };
                                            } else {
                                                newState[key] = {
                                                    ...val,
                                                    value: val.valueDefault
                                                        ? val.valueDefault.toString()
                                                        : '',
                                                    invalid: '',
                                                };
                                            }
                                        }
                                        return newState;
                                    });
                                } else {
                                    navigate(home, {
                                        replace: true,
                                    });
                                }
                            } else {
                                navigate(home, {
                                    replace: true,
                                });
                            }
                        })
                        .catch((error) => {
                            if (setSubmit) {
                                setSubmit(false);
                            }
                            apiCatch(error, setInput);
                        });
                } catch (error) {
                    if (setSubmit) {
                        setSubmit(false);
                    }
                    apiCatch(error, setInput);
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
        });
    };

    for (let [key, { gridSm = 4, ...val }] of Object.entries(input)) {
        mockup.push({
            form: { id: key, ...val },
            size: { sm: gridSm },
        });
    }

    return (
        <Paper
            elevation={0}
            // eslint-disable-next-line prettier/prettier
            sx={{ py: 1, px: 0, boxShadow: 'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px' }}>
            {typeIsUndefined(result.status) === true || result.status === 200 ? (
                <Box
                    component="form"
                    onSubmit={(e) => handleOnSubmit(e)}
                    autoComplete="off"
                    noValidate>
                    <Mockup
                        status={result.status}
                        input={input}
                        setInput={setInput}
                        pathname={location.pathname}
                    />

                    <Box sx={{ mx: '12px', mt: 1, mb: '4px' }}>
                        <Divider sx={{ mx: 0, mb: 1 }} />

                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            {result.status === 200 ? (
                                <>
                                    <Box>
                                        <MuiButton
                                            variant="outlined"
                                            color="error"
                                            onClick={() => {
                                                !submit &&
                                                    navigate(home, {
                                                        replace: true,
                                                    });
                                            }}
                                            children="Back"
                                            fullWidth={false}
                                            startIcon={<ArrowBackIcon />}
                                        />
                                        &nbsp;&nbsp;
                                        <MuiButton
                                            loadingButton={true}
                                            loadingProcess={submit}
                                            type="submit"
                                            color="success"
                                            children="Save"
                                            fullWidth={false}
                                            startIcon={<SaveIcon />}
                                        />
                                    </Box>

                                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                        {repeat === true && (
                                            <FormControlLabel
                                                name="create_another"
                                                checked={checked}
                                                control={
                                                    <Checkbox
                                                        size="small"
                                                        sx={{ p: 0, mr: '4px' }}
                                                    />
                                                }
                                                label={i18next.t('Create another')}
                                                labelPlacement="end"
                                                onChange={(e) => setChecked(e.target.checked)}
                                                sx={{
                                                    mx: 0,
                                                    '& .MuiTypography-root': {
                                                        fontSize: '14px',
                                                        userSelect: 'none',
                                                    },
                                                }}
                                            />
                                        )}
                                    </Box>
                                </>
                            ) : (
                                <>
                                    <Box sx={{ display: 'flex' }}>
                                        <Skeleton
                                            animation="wave"
                                            variant="rounded"
                                            width={100}
                                            height={31}
                                        />
                                        &nbsp;&nbsp;
                                        <Skeleton
                                            animation="wave"
                                            variant="rounded"
                                            width={100}
                                            height={31}
                                        />
                                    </Box>

                                    {type !== 'edit' && (
                                        <Skeleton
                                            animation="wave"
                                            variant="rounded"
                                            width={100}
                                            height={31}
                                        />
                                    )}
                                </>
                            )}
                        </Box>
                    </Box>
                </Box>
            ) : (
                <Error404 status={result.status} message={result.message} />
            )}
        </Paper>
    );
};

FormPageLoading.defaultProps = {
    repeat: false,
    payload: {},
};

FormPageLoading.propTypes = {
    home: PropTypes.string.isRequired,
    input: PropTypes.object.isRequired,
    setInput: PropTypes.func.isRequired,
    type: PropTypes.oneOf(['create', 'edit']).isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    action: PropTypes.string.isRequired,
    payload: PropTypes.object,
    repeat: PropTypes.bool,
};

export default FormPageLoading;

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useLocation, useNavigate } from 'react-router-dom';
import i18next from 'i18next';

import { ArrowBack as ArrowBackIcon, Save as SaveIcon } from '@mui/icons-material';
import { Box, Checkbox, Divider, FormControlLabel, Paper } from '@mui/material';
import { MuiButton } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import inputCreate from '@configs/inputCreate';
import inputEdit from '@configs/inputEdit';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { formValidation } from '@hooks/form';

import Mockup from './Mockup';

const FormPage = ({ home, input, setInput, type, method, action, repeat }) => {
    let navigate = useNavigate();
    let location = useLocation();

    const [submit, setSubmit] = useState(false);
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        if (type === 'edit') {
            setInput((prevState) => inputEdit(prevState, { data: location.state ?? {} }));
        } else {
            setInput((prevState) => inputCreate(prevState, {}));
        }
    }, []);

    const handleOnSubmit = (e) => {
        e.preventDefault();
        formValidation(input, setInput).then((resForm) => {
            setSubmit(true);
            const postData = async () => {
                try {
                    const formData = new FormData();
                    formData.append('_method', method);
                    for (const [key, value] of Object.entries(resForm)) {
                        formData.append(key, value);
                    }
                    await api
                        .post(home + action, formData, {
                            headers: {
                                ...(Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token') && {
                                    Authorization: xsrfToken().token,
                                }),
                            },
                        })
                        .then(({ data, statusText }) => {
                            setSubmit(false);
                            notistack['success'](i18next.t(data.message || statusText));
                            if (repeat) {
                                if (checked) {
                                    setInput((prevState) => {
                                        let newState = {};
                                        for (let [key, val] of Object.entries(prevState)) {
                                            if (val.type === 'select') {
                                                newState[key] = {
                                                    ...val,
                                                    value: val.valueDefault
                                                        ? val.valueDefault.toString()
                                                        : '',
                                                    options: data.data
                                                        ? data.data[key]
                                                        : val.options ?? [],
                                                    invalid: '',
                                                };
                                            } else {
                                                newState[key] = {
                                                    ...val,
                                                    value: val.valueDefault
                                                        ? val.valueDefault.toString()
                                                        : '',
                                                    invalid: '',
                                                };
                                            }
                                        }
                                        return newState;
                                    });
                                } else {
                                    navigate(home, {
                                        replace: true,
                                    });
                                }
                            } else {
                                navigate(home, {
                                    replace: true,
                                });
                            }
                        })
                        .catch((error) => {
                            if (setSubmit) setSubmit(false);
                            apiCatch(error, setInput);
                        });
                } catch (error) {
                    if (setSubmit) setSubmit(false);
                    apiCatch(error, setInput);
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
        });
    };
    return (
        <Paper
            elevation={0}
            // eslint-disable-next-line prettier/prettier
            sx={{ py: 1, px: 0, boxShadow: 'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px' }}>
            <Box component="form" onSubmit={(e) => handleOnSubmit(e)} autoComplete="off" noValidate>
                <Mockup
                    status={200}
                    input={input}
                    setInput={setInput}
                    pathname={location.pathname}
                />

                <Box sx={{ mx: '12px', mt: 1, mb: '4px' }}>
                    <Divider sx={{ mx: 0, mb: 1 }} />

                    <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Box>
                            <MuiButton
                                variant="outlined"
                                color="error"
                                onClick={() => {
                                    !submit &&
                                        navigate(home, {
                                            replace: true,
                                        });
                                }}
                                children="Back"
                                fullWidth={false}
                                startIcon={<ArrowBackIcon />}
                            />
                            &nbsp;&nbsp;
                            <MuiButton
                                loadingButton={true}
                                loadingProcess={submit}
                                type="submit"
                                color="success"
                                children="Save"
                                fullWidth={false}
                                startIcon={<SaveIcon />}
                            />
                        </Box>

                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            {repeat === true && (
                                <FormControlLabel
                                    name="create_another"
                                    checked={checked}
                                    control={<Checkbox size="small" sx={{ p: 0, mr: '4px' }} />}
                                    label={i18next.t('Create another')}
                                    labelPlacement="end"
                                    onChange={(e) => setChecked(e.target.checked)}
                                    sx={{
                                        mx: 0,
                                        '& .MuiTypography-root': {
                                            fontSize: '14px',
                                            userSelect: 'none',
                                        },
                                    }}
                                />
                            )}
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Paper>
    );
};

FormPage.defaultProps = {
    repeat: false,
};

FormPage.propTypes = {
    home: PropTypes.string.isRequired,
    input: PropTypes.object.isRequired,
    setInput: PropTypes.func.isRequired,
    type: PropTypes.oneOf(['create', 'edit']).isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    action: PropTypes.string.isRequired,
    repeat: PropTypes.bool,
};

export default FormPage;

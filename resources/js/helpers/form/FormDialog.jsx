import React, { forwardRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import { Close as CloseIcon, Save as SaveIcon } from '@mui/icons-material';
import {
    Box,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    Grow,
} from '@mui/material';
import { MuiButton } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import inputCreate from '@configs/inputCreate';
import inputEdit from '@configs/inputEdit';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { formValidation } from '@hooks/form';
import { typeIsFunction, typeIsObject } from '@hooks/type';

import Mockup from './Mockup';

const Transition = forwardRef(function Transition(props, ref) {
    const customProps = {
        ...props,
        timeout: {
            enter: 425,
            exit: 395,
        },
    };

    return <Grow ref={ref} {...customProps} />;
});

const FormDialog = ({
    open,
    width,
    title,
    setDialog,
    input,
    setInput,
    method,
    type,
    state,
    pathname,
    payload,
    repeat,
    reloadRow,
}) => {
    const [submit, setSubmit] = useState(false);
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        if (open === true) {
            if (type === 'edit') {
                // eslint-disable-next-line prettier/prettier
                setInput((prevState) => inputEdit(prevState, { data: typeIsObject(state) ? state : {} }));
            } else {
                setInput((prevState) => inputCreate(prevState, {}));
            }
        }
    }, [open]);

    const handleOnSubmit = (e) => {
        e.preventDefault();
        formValidation(input, setInput).then((resForm) => {
            setSubmit(true);
            const postData = async () => {
                try {
                    const formData = new FormData();
                    formData.append('_method', method);
                    for (const [key, value] of Object.entries({ ...payload, ...resForm })) {
                        formData.append(key, value);
                    }
                    await api
                        .post(pathname, formData, {
                            headers: {
                                ...(Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token') && {
                                    Authorization: xsrfToken().token,
                                }),
                            },
                        })
                        .then(({ data, statusText }) => {
                            setSubmit(false);
                            notistack['success'](i18next.t(data.message || statusText));
                            if (reloadRow && typeIsFunction(reloadRow)) reloadRow();
                            if (checked) {
                                setInput((prevState) => {
                                    let newState = {};
                                    for (let [key, val] of Object.entries(prevState)) {
                                        newState[key] = {
                                            ...val,
                                            value: val.valueDefault ?? '',
                                        };
                                    }
                                    return newState;
                                });
                            }
                            setDialog((prevState) => ({
                                ...prevState,
                                open: checked,
                            }));
                        })
                        .catch((error) => {
                            setSubmit(false);
                            apiCatch(error, setInput);
                        });
                } catch (error) {
                    setSubmit(false);
                    apiCatch(error, setInput);
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
        });
    };

    return (
        <Dialog
            fullWidth={true}
            open={open}
            maxWidth={width}
            transitionDuration={{ enter: 425, exit: 395 }}
            TransitionComponent={Transition}
            onClose={() =>
                !submit &&
                setDialog((prevState) => ({
                    ...prevState,
                    open: false,
                }))
            }
            keepMounted
            aria-describedby="alert-dialog-slide-description">
            <Box component="form" onSubmit={(e) => handleOnSubmit(e)} autoComplete="off" noValidate>
                <DialogTitle
                    sx={{
                        p: 1,
                        fontSize: '1.15rem',
                        textAlign: 'center',
                        borderBottom: 'solid 1px rgba(224, 224, 224, 1)',
                    }}
                    children={i18next.t(title)}
                />

                <DialogContent sx={{ py: '12px !important', px: '1px !important' }}>
                    <Mockup status={200} input={input} setInput={setInput} pathname={pathname} />
                </DialogContent>

                <DialogActions
                    sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        borderTop: 'solid 1px rgba(224, 224, 224, 1)',
                    }}>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        {repeat === true && (
                            <FormControlLabel
                                name="create_another"
                                checked={checked}
                                control={<Checkbox size="small" sx={{ p: 0, mr: '4px' }} />}
                                label={i18next.t('Create another')}
                                labelPlacement="end"
                                onChange={(e) => setChecked(e.target.checked)}
                                sx={{
                                    mx: 0,
                                    '& .MuiTypography-root': {
                                        fontSize: '14px',
                                        userSelect: 'none',
                                    },
                                }}
                            />
                        )}
                    </Box>

                    <Box>
                        <MuiButton
                            variant="outlined"
                            color="error"
                            onClick={() =>
                                !submit &&
                                setDialog((prevState) => ({
                                    ...prevState,
                                    open: false,
                                }))
                            }
                            children="Cancel"
                            fullWidth={false}
                            startIcon={<CloseIcon />}
                        />
                        &nbsp;
                        <MuiButton
                            loadingButton={true}
                            loadingProcess={submit}
                            type="submit"
                            color="success"
                            children="Save"
                            fullWidth={false}
                            startIcon={<SaveIcon />}
                        />
                    </Box>
                </DialogActions>
            </Box>
        </Dialog>
    );
};

FormDialog.defaultProps = {
    open: false,
    width: 'xs',
    title: '',
    method: configMethod[0],
    type: 'create',
    state: {},
    repeat: false,
    payload: {},
};

FormDialog.propTypes = {
    open: PropTypes.bool,
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
    title: PropTypes.string,
    setDialog: PropTypes.func.isRequired,
    input: PropTypes.object.isRequired,
    setInput: PropTypes.func.isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    type: PropTypes.oneOf(['create', 'edit']).isRequired,
    state: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    pathname: PropTypes.string,
    payload: PropTypes.object,
    repeat: PropTypes.bool,
    reloadRow: PropTypes.func,
};

export default FormDialog;

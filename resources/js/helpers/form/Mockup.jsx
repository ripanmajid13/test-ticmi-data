import React from 'react';
import PropTypes from 'prop-types';

import { Skeleton } from '@mui/material';
import { CCol, CContainer, CRow } from '@coreui/react';
import { CuiDate, CuiInput, CuiSelect, CuiTextarea } from '@components/coreui/forms';

import { RenderMap } from '@helpers/render';

const Mockup = ({ status, input, setInput, pathname }) => {
    let mockup = [];
    for (let [key, { gridSm = 4, ...val }] of Object.entries(input)) {
        mockup.push({
            form: { id: key, ...val },
            size: { sm: gridSm },
        });
    }

    return (
        <CContainer className="mw-100">
            <CRow>
                <RenderMap
                    data={mockup}
                    render={({ form, size }, key) => {
                        switch (form.type) {
                            // start select
                            case 'select':
                                return (
                                    <CCol xs={12} {...size} key={key}>
                                        <div className="mb-2">
                                            {status === 200 ? (
                                                <CuiSelect
                                                    pathname={pathname}
                                                    {...form}
                                                    setInput={setInput}
                                                />
                                            ) : (
                                                <>
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        width={form.label.length * 10 ?? 100}
                                                        height={22}
                                                        sx={{ mb: '2px' }}
                                                    />
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        height={31}
                                                    />
                                                    {form.note && (
                                                        <Skeleton
                                                            animation="wave"
                                                            variant="rounded"
                                                            sx={{ mt: '2.91px' }}
                                                            height={17}
                                                        />
                                                    )}
                                                </>
                                            )}
                                        </div>
                                    </CCol>
                                );
                            // end select

                            // start date
                            case 'date':
                                return (
                                    <CCol xs={12} {...size} key={key}>
                                        <div className="mb-2">
                                            {status === 200 ? (
                                                <CuiDate {...form} setInput={setInput} />
                                            ) : (
                                                <>
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        width={form.label.length * 10 ?? 100}
                                                        height={22}
                                                        sx={{ mb: '2px' }}
                                                    />
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        height={31}
                                                    />
                                                    {form.note && (
                                                        <Skeleton
                                                            animation="wave"
                                                            variant="rounded"
                                                            sx={{ mt: '2.91px' }}
                                                            height={17}
                                                        />
                                                    )}
                                                </>
                                            )}
                                        </div>
                                    </CCol>
                                );
                            // end date

                            // start textarea
                            case 'textarea':
                                return (
                                    <CCol xs={12} {...size} key={key}>
                                        <div className="mb-2">
                                            {status === 200 ? (
                                                <CuiTextarea {...form} setInput={setInput} />
                                            ) : (
                                                <>
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        width={form.label.length * 10 ?? 100}
                                                        height={22}
                                                        sx={{ mb: '2px' }}
                                                    />
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        height={
                                                            form.rows
                                                                ? form.rows < 2
                                                                    ? 35
                                                                    : (form.rows - 1) * 21 + 31
                                                                : 73
                                                        }
                                                    />
                                                    {form.note && (
                                                        <Skeleton
                                                            animation="wave"
                                                            variant="rounded"
                                                            sx={{ mt: '2.91px' }}
                                                            height={17}
                                                        />
                                                    )}
                                                </>
                                            )}
                                        </div>
                                    </CCol>
                                );
                            // end textarea

                            // start input
                            default:
                                return (
                                    <CCol xs={12} {...size} key={key}>
                                        <div className="mb-2">
                                            {status === 200 ? (
                                                <CuiInput {...form} setInput={setInput} />
                                            ) : (
                                                <>
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        width={form.label.length * 10 ?? 100}
                                                        height={22}
                                                        sx={{ mb: '2px' }}
                                                    />
                                                    <Skeleton
                                                        animation="wave"
                                                        variant="rounded"
                                                        height={31}
                                                    />
                                                    {form.note && (
                                                        <Skeleton
                                                            animation="wave"
                                                            variant="rounded"
                                                            sx={{ mt: '2.91px' }}
                                                            height={17}
                                                        />
                                                    )}
                                                </>
                                            )}
                                        </div>
                                    </CCol>
                                );
                            // end input
                        }
                    }}
                />
            </CRow>
        </CContainer>
    );
};

Mockup.propTypes = {
    status: PropTypes.number,
    input: PropTypes.object,
    setInput: PropTypes.func,
    pathname: PropTypes.string,
};

export default Mockup;

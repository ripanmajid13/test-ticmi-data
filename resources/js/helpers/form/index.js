import FormDialog from './FormDialog';
import FormDialogLoading from './FormDialogLoading';
import FormPage from './FormPage';
import FormPageLoading from './FormPageLoading';

export { FormDialog, FormDialogLoading, FormPage, FormPageLoading };

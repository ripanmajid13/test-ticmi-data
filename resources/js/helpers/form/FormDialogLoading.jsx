import React, { forwardRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import { Close as CloseIcon, Save as SaveIcon } from '@mui/icons-material';
import {
    Box,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    Grow,
    Skeleton,
} from '@mui/material';
import { MuiButton } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import inputCreate from '@configs/inputCreate';
import inputEdit from '@configs/inputEdit';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { formValidation } from '@hooks/form';
import { typeIsFunction, typeIsUndefined } from '@hooks/type';
import Error404 from '@views/errors/Error404';

import Mockup from './Mockup';

const Transition = forwardRef(function Transition(props, ref) {
    const customProps = {
        ...props,
        timeout: {
            enter: 425,
            exit: 395,
        },
    };

    return <Grow ref={ref} {...customProps} />;
});

const FormDialogLoading = ({
    open,
    width,
    title,
    setDialog,
    input,
    setInput,
    method,
    type,
    pathname,
    payload,
    repeat,
    uri,
    params,
    action,
    reloadRow,
}) => {
    const [res, setRes] = useState({});
    const [submit, setSubmit] = useState(false);
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        if (open === true) {
            let mounted = true;
            setRes({});
            const postData = async () => {
                try {
                    let headers = {};
                    const formData = new FormData();

                    if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                        headers['Authorization'] = xsrfToken().token;
                    }

                    formData.append('_method', 'POST');
                    for (const [key, value] of Object.entries(payload)) {
                        formData.append(key, value);
                    }
                    await api
                        .post(pathname, formData, { headers: headers })
                        .then((response) => {
                            if (mounted) {
                                if (type === 'edit') {
                                    // eslint-disable-next-line prettier/prettier
                                    setInput((prevState) => inputEdit(prevState, response.data ?? {}));
                                } else {
                                    // eslint-disable-next-line prettier/prettier
                                    setInput((prevState) => inputCreate(prevState, response.data ?? {}));
                                }

                                setRes({
                                    data: response.data,
                                    status: response.status,
                                    statusText: response.statusText,
                                    headers: response.headers,
                                });
                            }
                        })
                        .catch(
                            ({ response }) =>
                                mounted &&
                                setRes({
                                    status: response.status,
                                    message: response.data.message ?? response.statusText,
                                })
                        );
                } catch ({ response }) {
                    mounted &&
                        setRes({
                            status: response.status,
                            message: response.data.message ?? response.statusText,
                        });
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
            return () => (mounted = false);
        }
    }, [open]);

    const handleOnSubmit = (e) => {
        e.preventDefault();
        formValidation(input, setInput).then((resForm) => {
            setSubmit(true);
            const postData = async () => {
                try {
                    const formData = new FormData();
                    formData.append('_method', method);
                    for (const [key, value] of Object.entries({ ...payload, ...resForm })) {
                        formData.append(key, value);
                    }
                    await api
                        .post(uri + (params.length < 1 ? '' : '/' + params) + action, formData, {
                            headers: {
                                ...(Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token') && {
                                    Authorization: xsrfToken().token,
                                }),
                            },
                        })
                        .then(({ data, statusText }) => {
                            setSubmit(false);
                            notistack['success'](i18next.t(data.message || statusText));
                            if (reloadRow && typeIsFunction(reloadRow)) reloadRow();
                            if (checked) {
                                setInput((prevState) => {
                                    let newState = {};
                                    for (let [key, val] of Object.entries(prevState)) {
                                        newState[key] = {
                                            ...val,
                                            value: val.valueDefault ?? '',
                                        };
                                    }
                                    return newState;
                                });
                            }
                            setDialog((prevState) => ({
                                ...prevState,
                                open: checked,
                            }));
                        })
                        .catch((error) => {
                            setSubmit(false);
                            apiCatch(error, setInput);
                        });
                } catch (error) {
                    setSubmit(false);
                    apiCatch(error, setInput);
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
        });
    };

    return (
        <Dialog
            fullWidth={true}
            open={open}
            maxWidth={width}
            transitionDuration={{ enter: 425, exit: 395 }}
            TransitionComponent={Transition}
            onClose={() =>
                !submit &&
                setDialog((prevState) => ({
                    ...prevState,
                    open: false,
                }))
            }
            keepMounted
            aria-describedby="alert-dialog-slide-description">
            <DialogTitle
                sx={{
                    p: 1,
                    fontSize: '1.15rem',
                    textAlign: 'center',
                    borderBottom: 'solid 1px rgba(224, 224, 224, 1)',
                }}
                children={i18next.t(title)}
            />
            {typeIsUndefined(res.status) === true || res.status === 200 ? (
                <Box
                    component="form"
                    onSubmit={(e) => handleOnSubmit(e)}
                    autoComplete="off"
                    noValidate>
                    <DialogContent sx={{ py: '12px !important', px: '1px !important' }}>
                        <Mockup
                            status={res.status}
                            input={input}
                            setInput={setInput}
                            pathname={pathname}
                        />
                    </DialogContent>

                    <DialogActions
                        sx={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            borderTop: 'solid 1px rgba(224, 224, 224, 1)',
                        }}>
                        <Box>
                            {res.status === 200 ? (
                                <>
                                    <MuiButton
                                        variant="outlined"
                                        color="error"
                                        onClick={() =>
                                            !submit &&
                                            setDialog((prevState) => ({
                                                ...prevState,
                                                open: false,
                                            }))
                                        }
                                        children="Cancel"
                                        fullWidth={false}
                                        startIcon={<CloseIcon />}
                                    />
                                    &nbsp;
                                    <MuiButton
                                        loadingButton={true}
                                        loadingProcess={submit}
                                        type="submit"
                                        color="success"
                                        children="Save"
                                        fullWidth={false}
                                        startIcon={<SaveIcon />}
                                    />
                                </>
                            ) : (
                                <Box sx={{ display: 'flex' }}>
                                    <Skeleton
                                        animation="wave"
                                        variant="rounded"
                                        width={100}
                                        height={31}
                                    />
                                    &nbsp;
                                    <Skeleton
                                        animation="wave"
                                        variant="rounded"
                                        width={100}
                                        height={31}
                                    />
                                </Box>
                            )}
                        </Box>

                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            {repeat === true &&
                                (res.status === 200 ? (
                                    <FormControlLabel
                                        name="create_another"
                                        checked={checked}
                                        control={<Checkbox size="small" sx={{ p: 0, mr: '4px' }} />}
                                        label={i18next.t('Create another')}
                                        labelPlacement="end"
                                        onChange={(e) => setChecked(e.target.checked)}
                                        sx={{
                                            mx: 0,
                                            '& .MuiTypography-root': {
                                                fontSize: '14px',
                                                userSelect: 'none',
                                            },
                                        }}
                                    />
                                ) : (
                                    <Skeleton
                                        animation="wave"
                                        variant="rounded"
                                        width={100}
                                        height={31}
                                    />
                                ))}
                        </Box>
                    </DialogActions>
                </Box>
            ) : (
                <Box>
                    <Error404 status={res.status} message={res.message} />
                    <DialogActions
                        sx={{
                            mb: 2,
                            backgroundColor: '#FFFFFF',
                            display: 'flex',
                            justifyContent: 'center',
                        }}>
                        <MuiButton
                            variant="outlined"
                            color="error"
                            onClick={() =>
                                !submit &&
                                setDialog((prevState) => ({
                                    ...prevState,
                                    open: false,
                                }))
                            }
                            children="Close"
                            fullWidth={false}
                            startIcon={<CloseIcon />}
                        />
                    </DialogActions>
                </Box>
            )}
        </Dialog>
    );
};

FormDialogLoading.defaultProps = {
    open: false,
    width: 'xs',
    title: '',
    method: configMethod[0],
    type: 'create',
    repeat: false,
    uri: '',
    params: '',
    action: '',
    payload: {},
};

FormDialogLoading.propTypes = {
    open: PropTypes.bool,
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
    title: PropTypes.string,
    setDialog: PropTypes.func.isRequired,
    input: PropTypes.object.isRequired,
    setInput: PropTypes.func.isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    type: PropTypes.oneOf(['create', 'edit']).isRequired,
    pathname: PropTypes.string,
    payload: PropTypes.object,
    repeat: PropTypes.bool,
    uri: PropTypes.string,
    params: PropTypes.string,
    action: PropTypes.string,
    reloadRow: PropTypes.func,
};

export default FormDialogLoading;

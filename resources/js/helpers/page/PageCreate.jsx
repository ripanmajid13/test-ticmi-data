import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useLocation, useNavigate } from 'react-router-dom';

import { ArrowBack as ArrowBackIcon, Save as SaveIcon } from '@mui/icons-material';
import { Box, Divider, Paper } from '@mui/material';
import { CContainer, CRow } from '@coreui/react';
import { MuiButton } from '@components/materialui/inputs';

const PageCreate = ({ children, home, input, repeat }) => {
    let navigate = useNavigate();
    let location = useLocation();

    const [submit, setSubmit] = useState(false);

    const handleOnSubmit = (e) => {
        e.preventDefault();

        if (location.pathname.replace(home, '').includes('/edit') === true) {
            console.log('PUT');
        } else {
            console.log('POST');
        }
    };

    return (
        <Paper
            elevation={0}
            sx={{
                py: 1,
                px: 0,
                // eslint-disable-next-line prettier/prettier
                boxShadow: 'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px',
            }}>
            <Box component="form" onSubmit={(e) => handleOnSubmit(e)} autoComplete="off" noValidate>
                <CContainer className="mw-100">
                    <CRow>{children}</CRow>
                </CContainer>

                <Box sx={{ mx: '12px', mt: 1, mb: '4px' }}>
                    <Divider sx={{ mx: 0, mb: 1 }} />

                    <Box>
                        {home && (
                            <MuiButton
                                variant="outlined"
                                color="error"
                                onClick={() => {
                                    !submit &&
                                        navigate(home, {
                                            replace: true,
                                        });
                                }}
                                children="Back"
                                fullWidth={false}
                                startIcon={<ArrowBackIcon />}
                            />
                        )}
                        &nbsp;&nbsp;
                        <MuiButton
                            loadingButton={true}
                            loadingProcess={submit}
                            type="submit"
                            color="success"
                            children="Save"
                            fullWidth={false}
                            startIcon={<SaveIcon />}
                        />
                    </Box>

                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        {/* {repeat === true && (
                            <FormControlLabel
                                checked={checked}
                                control={<Checkbox size="small" sx={{ p: 0, mr: '4px' }} />}
                                label={i18next.t('Create another')}
                                labelPlacement="end"
                                onChange={(e) => setChecked(e.target.checked)}
                                sx={{
                                    mx: 0,
                                    '& .MuiTypography-root': {
                                        fontSize: '14px',
                                        userSelect: 'none',
                                    },
                                }}
                            />
                        )} */}
                    </Box>
                </Box>
            </Box>
        </Paper>
    );
};

PageCreate.propTypes = {
    children: PropTypes.node.isRequired,
    home: PropTypes.string.isRequired,
    input: PropTypes.object,
    repeat: PropTypes.bool,
};

export default PageCreate;

import React, { forwardRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import {
    AccountCircle as AccountCircleIcon,
    Check as CheckIcon,
    Close as CloseIcon,
    Lock as LockIcon,
    Visibility as VisibilityIcon,
    VisibilityOff as VisibilityOffIcon,
} from '@mui/icons-material';
import {
    Alert,
    Box,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grow,
    IconButton,
    InputAdornment,
    Typography,
} from '@mui/material';
import { MuiButton, MuiTextField } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { formValidation } from '@hooks/form';
import { typeIsFunction } from '@hooks/type';

const Transition = forwardRef(function Transition(props, ref) {
    const customProps = {
        ...props,
        timeout: {
            enter: 425,
            exit: 395,
        },
    };

    return <Grow ref={ref} {...customProps} />;
});

const DialogAuth = ({
    open,
    width,
    method,
    setDialog,
    title,
    text,
    pathname,
    payload,
    reloadRow,
}) => {
    const [error, setError] = useState('');
    const [submit, setSubmit] = useState(false);
    const [formData, setFormData] = useState({
        username: {
            value: '',
            label: 'Username',
            labelRequired: true,
            required: true,
            placeholder: 'Username/Email',
        },
        password: {
            value: '',
            valueType: 'password',
            valueEncrypt: true,
            type: 'password',
            label: 'Password',
            labelRequired: true,
            required: true,
        },
    });

    useEffect(() => {
        if (open === true) {
            setError('');
            setFormData((prevState) => ({
                username: {
                    ...prevState.username,
                    value: '',
                    invalid: '',
                },
                password: {
                    ...prevState.password,
                    value: '',
                    valueType: 'password',
                    invalid: '',
                },
            }));
        }
    }, [open]);

    const handleOnSubmit = (e) => {
        e.preventDefault();

        setError('');
        if (configMethod.some((ms) => ms === method)) {
            formValidation(formData, setFormData).then((resForm) => {
                if (setSubmit) {
                    setSubmit(true);
                }
                const postData = async () => {
                    try {
                        const formData = new FormData();

                        formData.append('_method', method);
                        for (const [key, value] of Object.entries({ ...resForm, ...payload })) {
                            formData.append(key, value);
                        }

                        await api
                            .post(pathname, formData, {
                                headers: {
                                    ...(Object.keys(xsrfToken()).some(
                                        (xrfs) => xrfs === 'token'
                                    ) && {
                                        Authorization: xsrfToken().token,
                                    }),
                                },
                            })
                            .then(({ data, statusText }) => {
                                notistack['success'](i18next.t(data.message || statusText));
                                if (reloadRow && typeIsFunction(reloadRow)) {
                                    reloadRow();
                                }
                                if (setSubmit) {
                                    setSubmit(false);
                                }
                                setDialog((prevState) => ({
                                    ...prevState,
                                    open: false,
                                }));
                            })
                            .catch((error) => {
                                if (setSubmit) {
                                    setSubmit(false);
                                }
                                apiCatch(error, setFormData, setError);
                            });
                    } catch (error) {
                        if (setSubmit) {
                            setSubmit(false);
                        }
                        apiCatch(error, setFormData, setError);
                    }
                };
                setTimeout(() => {
                    postData();
                }, 1000);
            });
        } else {
            notistack['success'](i18next.t('Method not found.'));
        }
    };

    return (
        <Dialog
            fullWidth={true}
            open={open}
            maxWidth={width}
            transitionDuration={{ enter: 425, exit: 395 }}
            TransitionComponent={Transition}
            onClose={() =>
                !submit &&
                setDialog((prevState) => ({
                    ...prevState,
                    open: false,
                }))
            }
            keepMounted
            aria-describedby="alert-dialog-slide-description">
            <Box component="form" onSubmit={(e) => handleOnSubmit(e)} autoComplete="off" noValidate>
                <DialogTitle
                    sx={{
                        p: 1,
                        fontSize: '1.15rem',
                        textAlign: 'center',
                        borderBottom: 'solid 1px rgba(224, 224, 224, 1)',
                    }}
                    children={i18next.t(title)}
                />

                <DialogContent>
                    {error.length > 0 && (
                        <Alert
                            severity="error"
                            sx={{ my: '20px', width: '100%' }}
                            onClose={() => setError('')}
                            children={i18next.t(error)}
                        />
                    )}

                    {text.length > 0 && (
                        <Typography
                            variant="subtitle1"
                            gutterBottom
                            className="mt-3"
                            sx={{ textAlign: 'center' }}
                            children={i18next.t(text) + ' ?'}
                        />
                    )}

                    <div className="mt-4 mb-4">
                        <MuiTextField
                            id="username"
                            {...formData.username}
                            setInput={setFormData}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircleIcon />
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </div>

                    <div>
                        <MuiTextField
                            id="password"
                            {...formData.password}
                            setInput={setFormData}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <LockIcon />
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={() => {
                                                setFormData({
                                                    ...formData,
                                                    password: {
                                                        ...formData.password,
                                                        valueType:
                                                            formData.password.valueType == 'text'
                                                                ? 'password'
                                                                : 'text',
                                                    },
                                                });
                                            }}
                                            edge="end">
                                            {formData.password.valueType == 'text' ? (
                                                <VisibilityOffIcon />
                                            ) : (
                                                <VisibilityIcon />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </div>
                </DialogContent>

                <DialogActions
                    sx={{
                        justifyContent: 'center',
                        borderTop: 'solid 1px rgba(224, 224, 224, 1)',
                    }}>
                    <MuiButton
                        variant="outlined"
                        color="error"
                        onClick={() =>
                            !submit &&
                            setDialog((prevState) => ({
                                ...prevState,
                                open: false,
                            }))
                        }
                        children="Cancel"
                        fullWidth={false}
                        startIcon={<CloseIcon />}
                    />
                    &nbsp;
                    <MuiButton
                        loadingButton={true}
                        loadingProcess={submit}
                        type="submit"
                        color="success"
                        children="Yes"
                        fullWidth={false}
                        startIcon={<CheckIcon />}
                    />
                </DialogActions>
            </Box>
        </Dialog>
    );
};

DialogAuth.defaultProps = {
    open: false,
    width: 'xs',
    method: configMethod[0],
    title: '',
    text: '',
    pathname: '',
    payload: {},
};

DialogAuth.propTypes = {
    open: PropTypes.bool.isRequired,
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']).isRequired,
    setDialog: PropTypes.func.isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    title: PropTypes.string,
    text: PropTypes.string,
    pathname: PropTypes.string,
    payload: PropTypes.object,
    reloadRow: PropTypes.func,
};

export default DialogAuth;

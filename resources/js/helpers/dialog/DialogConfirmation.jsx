import React, { forwardRef, useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import { Check as CheckIcon, Close as CloseIcon } from '@mui/icons-material';
import {
    Box,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grow,
    Typography,
} from '@mui/material';
import { MuiButton } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { typeIsFunction } from '@hooks/type';

const Transition = forwardRef(function Transition(props, ref) {
    const customProps = {
        ...props,
        timeout: {
            enter: 425,
            exit: 395,
        },
    };

    return <Grow ref={ref} {...customProps} />;
});

const DialogConfirmation = ({
    open,
    width,
    method,
    setDialog,
    title,
    text,
    pathname,
    payload,
    reloadRow,
}) => {
    const [submit, setSubmit] = useState(false);

    const handleOnSubmit = (e) => {
        e.preventDefault();

        if (configMethod.some((ms) => ms === method)) {
            if (setSubmit) {
                setSubmit(true);
            }
            const postData = async () => {
                try {
                    await api
                        .post(
                            pathname,
                            { _method: method, ...payload },
                            {
                                headers: {
                                    ...(Object.keys(xsrfToken()).some(
                                        (xrfs) => xrfs === 'token'
                                    ) && {
                                        Authorization: xsrfToken().token,
                                    }),
                                },
                            }
                        )
                        .then(({ data, statusText }) => {
                            notistack['success'](i18next.t(data.message || statusText));
                            if (reloadRow && typeIsFunction(reloadRow)) {
                                reloadRow();
                            }
                            if (setSubmit) {
                                setSubmit(false);
                            }
                            setDialog((prevState) => ({
                                ...prevState,
                                open: false,
                            }));
                        })
                        .catch((error) => {
                            if (reloadRow && typeIsFunction(reloadRow)) {
                                reloadRow();
                            }
                            if (setSubmit) {
                                setSubmit(false);
                            }
                            apiCatch(error);
                            setDialog((prevState) => ({
                                ...prevState,
                                open: false,
                            }));
                        });
                } catch (error) {
                    if (setSubmit) {
                        setSubmit(false);
                    }
                    apiCatch(error);
                    setDialog((prevState) => ({
                        ...prevState,
                        open: false,
                    }));
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
        } else {
            notistack['success'](i18next.t('Method not found.'));
        }
    };

    return (
        <Dialog
            fullWidth={true}
            open={open}
            maxWidth={width}
            transitionDuration={{ enter: 425, exit: 395 }}
            TransitionComponent={Transition}
            onClose={() =>
                !submit &&
                setDialog((prevState) => ({
                    ...prevState,
                    open: false,
                }))
            }
            keepMounted
            aria-describedby="alert-dialog-slide-description">
            <Box component="form" onSubmit={(e) => handleOnSubmit(e)} autoComplete="off" noValidate>
                <DialogTitle
                    sx={{
                        p: 1,
                        fontSize: '1.15rem',
                        textAlign: 'center',
                        borderBottom: 'solid 1px rgba(224, 224, 224, 1)',
                    }}
                    children={i18next.t(title)}
                />

                <DialogContent>
                    <Typography
                        variant="subtitle1"
                        gutterBottom
                        className="mt-3 mb-0"
                        sx={{ textAlign: 'center' }}
                        children={i18next.t(text) + ' ?'}
                    />
                </DialogContent>

                <DialogActions
                    sx={{
                        justifyContent: 'center',
                        borderTop: 'solid 1px rgba(224, 224, 224, 1)',
                    }}>
                    <MuiButton
                        variant="outlined"
                        color="error"
                        onClick={() =>
                            !submit &&
                            setDialog((prevState) => ({
                                ...prevState,
                                open: false,
                            }))
                        }
                        children="Cancel"
                        fullWidth={false}
                        startIcon={<CloseIcon />}
                    />
                    &nbsp;
                    <MuiButton
                        loadingButton={true}
                        loadingProcess={submit}
                        type="submit"
                        color="success"
                        children="Yes"
                        fullWidth={false}
                        startIcon={<CheckIcon />}
                    />
                </DialogActions>
            </Box>
        </Dialog>
    );
};

DialogConfirmation.defaultProps = {
    open: false,
    width: 'xs',
    method: configMethod[0],
    title: '',
    text: '',
    pathname: '',
    payload: {},
};

DialogConfirmation.propTypes = {
    open: PropTypes.bool.isRequired,
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']).isRequired,
    setDialog: PropTypes.func.isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    title: PropTypes.string,
    text: PropTypes.string,
    pathname: PropTypes.string,
    payload: PropTypes.object,
    reloadRow: PropTypes.func,
};

// DialogConfirmation.defaultProps = {
//     open: false,
//     width: 'xs',
//     title: '',
//     text: '',
//     payload: {},
// };

// DialogConfirmation.propTypes = {
//     open: PropTypes.bool,
//     width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
//     title: PropTypes.string,
//     text: PropTypes.string,
//     setDialog: PropTypes.func.isRequired,
//     reloadRow: PropTypes.func,
//     method: PropTypes.string,
//     pathname: PropTypes.string,
//     payload: PropTypes.object,
// };

export default DialogConfirmation;

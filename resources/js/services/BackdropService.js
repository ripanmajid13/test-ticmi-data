import i18next from 'i18next';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { typeIsFunction, typeIsObject, typeIsString } from '@hooks/type';
import store from '@store';
import { STORE_TYPE_APP_STATE } from '@store/type/app';

const ValidationService = ({
    setInput,
    setError,
    input = {},
    title = 'Loading',
    uri = window.location.pathname,
}) => {
    return new Promise((resolve) => {
        if (input && !typeIsObject(input)) {
            notistack['error'](i18next.t('input must be object.'));
        } else if (setInput && !typeIsFunction(setInput)) {
            notistack['error'](i18next.t('setInput must be function.'));
        } else if (setError && !typeIsFunction(setError)) {
            notistack['error'](i18next.t('setError must be function.'));
        } else if (!typeIsString(title)) {
            notistack['error'](i18next.t('title must be function.'));
        } else if (!typeIsString(uri)) {
            notistack['error'](i18next.t('uri must be function.'));
        } else {
            resolve({ input, setInput, setError, title, uri });
        }
    });
};

const BackdropServicePost = (props) => {
    return new Promise((resolve) => {
        ValidationService(props).then(({ input, setInput, setError, title, uri }) => {
            store.dispatch({
                type: STORE_TYPE_APP_STATE,
                app_backdrop_open: true,
                app_backdrop_content: i18next.t(title) + ' . . .',
            });

            const postData = async () => {
                try {
                    let headers = {};
                    const payload = new FormData();

                    if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                        headers['Authorization'] = xsrfToken().token;
                    }

                    payload.append('_method', 'POST');
                    for (const [key, value] of Object.entries(input)) {
                        payload.append(key, value);
                    }
                    await api
                        .post(uri, payload, { headers: headers })
                        .then((response) => {
                            resolve({
                                data: response.data,
                                status: response.status,
                                statusText: response.statusText,
                                headers: response.headers,
                            });
                        })
                        .catch((error) => {
                            apiCatch(error, setInput, setError);
                        });
                } catch (error) {
                    store.dispatch({
                        type: STORE_TYPE_APP_STATE,
                        app_backdrop_open: false,
                    });
                    apiCatch(error, setInput, setError);
                }
            };
            setTimeout(() => {
                postData();
            }, 1000);
        });
    });
};

const BackdropServicePut = (props) => {
    return new Promise((resolve) => {
        ValidationService(props).then(({ input, setInput, setError, title, uri }) => {
            store.dispatch({
                type: STORE_TYPE_APP_STATE,
                app_backdrop_open: true,
                app_backdrop_content: i18next.t(title) + ' . . .',
            });
            const putData = async () => {
                try {
                    let headers = {};
                    const payload = new FormData();

                    if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                        headers['Authorization'] = xsrfToken().token;
                    }

                    payload.append('_method', 'PUT');
                    for (const [key, value] of Object.entries(input)) {
                        payload.append(key, value);
                    }
                    await api
                        .post(uri, payload, { headers: headers })
                        .then((response) => {
                            resolve({
                                data: response.data,
                                status: response.status,
                                statusText: response.statusText,
                                headers: response.headers,
                            });
                        })
                        .catch((error) => {
                            apiCatch(error, setInput, setError);
                        });
                } catch (error) {
                    store.dispatch({
                        type: STORE_TYPE_APP_STATE,
                        app_backdrop_open: false,
                    });
                    apiCatch(error, setInput, setError);
                }
            };
            setTimeout(() => {
                putData();
            }, 1000);
        });
    });
};

const BackdropServiceDelete = (props) => {
    return new Promise((resolve) => {
        ValidationService(props).then(({ input, setInput, setError, title, uri }) => {
            store.dispatch({
                type: STORE_TYPE_APP_STATE,
                app_backdrop_open: true,
                app_backdrop_content: i18next.t(title) + ' . . .',
            });
            const deleteData = async () => {
                try {
                    let headers = {};
                    const payload = new FormData();

                    if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                        headers['Authorization'] = xsrfToken().token;
                    }

                    payload.append('_method', 'DELETE');
                    for (const [key, value] of Object.entries(input)) {
                        payload.append(key, value);
                    }
                    await api
                        .post(uri, payload, { headers: headers })
                        .then((response) => {
                            resolve({
                                data: response.data,
                                status: response.status,
                                statusText: response.statusText,
                                headers: response.headers,
                            });
                        })
                        .catch((error) => {
                            apiCatch(error, setInput, setError);
                        });
                } catch (error) {
                    store.dispatch({
                        type: STORE_TYPE_APP_STATE,
                        app_backdrop_open: false,
                    });
                    apiCatch(error, setInput, setError);
                }
            };
            setTimeout(() => {
                deleteData();
            }, 1000);
        });
    });
};

export { BackdropServiceDelete, BackdropServicePost, BackdropServicePut };

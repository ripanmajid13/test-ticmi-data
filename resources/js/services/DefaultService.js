import i18next from 'i18next';

import api from '@configs/api';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { typeIsBoolean, typeIsFunction, typeIsObject, typeIsString } from '@hooks/type';

// eslint-disable-next-line prettier/prettier
const MockupService = ({ method, mounted = true, url = window.location.pathname, payload = {}, setRes }) => {
    if (!typeIsBoolean(mounted)) {
        notistack['error'](i18next.t('mounted must be boolean.'));
    } else if (!typeIsString(url)) {
        notistack['error'](i18next.t('url must be string.'));
    } else if (!typeIsObject(payload)) {
        notistack['error'](i18next.t('payload must be object.'));
    } else if (!typeIsFunction(setRes)) {
        notistack['error'](i18next.t('setRes must be function.'));
    } else {
        const postData = async () => {
            try {
                let headers = {};
                const formData = new FormData();
                if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                    headers['Authorization'] = xsrfToken().token;
                }
                formData.append('_method', method);
                for (const [key, value] of Object.entries(payload)) {
                    formData.append(key, value);
                }
                await api
                    .post(url, formData, { headers: headers })
                    .then(
                        (response) =>
                            mounted &&
                            setRes({
                                data: response.data,
                                status: response.status,
                                statusText: response.statusText,
                                headers: response.headers,
                            })
                    )
                    .catch(
                        (error) =>
                            mounted &&
                            setRes({
                                data: error.response.data,
                                status: error.response.status,
                                statusText: error.response.statusText,
                                headers: error.response.headers,
                            })
                    );
            } catch (error) {
                mounted &&
                    setRes({
                        data: error.response.data,
                        status: error.response.status,
                        statusText: error.response.statusText,
                        headers: error.response.headers,
                    });
            }
        };

        setTimeout(() => {
            postData();
        }, 1000);
    }
};

export const DefaultServicePost = (props) => MockupService({ ...props, method: 'POST' });
export const DefaultServicePut = (props) => MockupService({ ...props, method: 'PUT' });
export const DefaultServiceDelete = (props) => MockupService({ ...props, method: 'DELETE' });

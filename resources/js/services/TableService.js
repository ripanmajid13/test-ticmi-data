import i18next from 'i18next';

import api from '@configs/api';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import {
    typeIsArray,
    typeIsBoolean,
    typeIsFunction,
    typeIsObject,
    typeIsString,
    typeIsUndefined,
} from '@hooks/type';

// eslint-disable-next-line prettier/prettier
const TableService = async ({ mounted = true, service: { uri = '', params = {}, paramsDefault = {}, payload = {}, column = [] }, setRes }) => {
    if (!typeIsBoolean(mounted)) {
        notistack['error'](i18next.t('mounted must be boolean.'));
    } else if (!typeIsString(uri) || (typeIsString(uri) && uri.length < 1)) {
        notistack['error'](i18next.t('uri must be string.'));
    } else if (!typeIsObject(params)) {
        notistack['error'](i18next.t('params must be object.'));
    } else if (!typeIsObject(paramsDefault)) {
        notistack['error'](i18next.t('paramsDefault must be object.'));
    } else if (!typeIsObject(payload)) {
        notistack['error'](i18next.t('payload must be object.'));
    } else if (!typeIsArray(column)) {
        notistack['error'](i18next.t('column must be array.'));
    } else if (!typeIsFunction(setRes)) {
        notistack['error'](i18next.t('setRes must be function.'));
    } else {
        try {
            let headers = {};
            let setParams = '';
            const formData = new FormData();

            if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                headers['Authorization'] = xsrfToken().token;
            }

            if (!payload.datatable_filter) {
                formData.append(
                    'datatable_filter',
                    JSON.stringify(
                        column
                            .filter(
                                (cf) =>
                                    cf.filter === true &&
                                    typeIsUndefined(cf.filterValue) !== true &&
                                    typeIsString(cf.filterValue) === true &&
                                    cf.filterValue.length > 0
                            )
                            .map((valFil) => {
                                return {
                                    key: valFil.accessor,
                                    value: valFil.filterValue,
                                    type: valFil.filterType ?? 'text',
                                };
                            })
                    )
                );
            }

            for (const [key, value] of Object.entries(payload)) {
                formData.append(key, value);
            }

            if (!Object.keys(payload).some((pay) => pay === 'paginate')) {
                formData.append('paginate', 10);
            }

            if (Object.keys({ ...paramsDefault, ...params }).length > 0) {
                let getParams = [];
                for (let [key, val] of Object.entries({ ...paramsDefault, ...params })) {
                    getParams.push(`${key}=${val}`);
                }
                if (getParams.length > 0) {
                    setParams = '?' + getParams.join('&&');
                }
            }

            await api
                .post(uri + setParams, formData, { headers: headers })
                .then(
                    (response) =>
                        mounted &&
                        setRes({
                            data: response.data,
                            status: response.status,
                            statusText: response.statusText,
                            headers: response.headers,
                        })
                )
                .catch(
                    (error) =>
                        mounted &&
                        setRes({
                            data: error.response.data,
                            status: error.response.status,
                            statusText: error.response.statusText,
                            headers: error.response.headers,
                        })
                );
        } catch (error) {
            mounted &&
                setRes({
                    data: error.response.data,
                    status: error.response.status,
                    statusText: error.response.statusText,
                    headers: error.response.headers,
                });
        }
    }
};

export default TableService;

import i18next from 'i18next';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { formValidation } from '@hooks/form';
import { typeIsFunction, typeIsObject, typeIsString } from '@hooks/type';

// eslint-disable-next-line prettier/prettier
const MockupService = ({ method, url = window.location.pathname, input = {}, setInput, setSubmit, setError }) => {
    return new Promise((resolve) => {
        if (!typeIsString(url)) {
            notistack['error'](i18next.t('url must be string.'));
        } else if (!typeIsObject(input)) {
            notistack['error'](i18next.t('input must be object.'));
        } else if (!typeIsFunction(setInput)) {
            notistack['error'](i18next.t('setInput must be function.'));
        } else if (setSubmit && !typeIsFunction(setSubmit)) {
            notistack['error'](i18next.t('setSubmit must be function.'));
        } else if (setError && !typeIsFunction(setError)) {
            notistack['error'](i18next.t('setError must be function.'));
        } else {
            if (setError) setError('');
            formValidation(input, setInput).then((resForm) => {
                if (setSubmit) setSubmit(true);

                const postData = async () => {
                    try {
                        let headers = {};
                        const payload = new FormData();
                        if (Object.keys(xsrfToken()).some((xrfs) => xrfs === 'token')) {
                            headers['Authorization'] = xsrfToken().token;
                        }
                        payload.append('_method', method);
                        for (const [key, value] of Object.entries(resForm)) {
                            payload.append(key, value);
                        }
                        await api
                            .post(url, payload, { headers: headers })
                            .then((response) => {
                                resolve({
                                    data: response.data,
                                    status: response.status,
                                    statusText: response.statusText,
                                    headers: response.headers,
                                });
                            })
                            .catch((error) => {
                                if (setSubmit) setSubmit(false);
                                apiCatch(error, setInput, setError);
                            });
                    } catch (error) {
                        if (setSubmit) setSubmit(false);
                        apiCatch(error, setInput, setError);
                    }
                };

                setTimeout(() => {
                    postData();
                }, 500);
            });
        }
    });
};

export const FormServicePost = (props) => MockupService({ ...props, method: 'POST' });
export const FormServicePut = (props) => MockupService({ ...props, method: 'PUT' });
export const FormServiceDelete = (props) => MockupService({ ...props, method: 'DELETE' });

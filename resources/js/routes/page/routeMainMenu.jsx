import React from 'react';

// icon
import DashboardIcon from '@mui/icons-material/Dashboard';

import DaftarPelanggan from '@views/DaftarPelanggan';
import DaftarPelangganProfile from '@views/DaftarPelanggan/Profile';

const routeMainMenu = [
    {
        name: 'Pelanggan',
        path: '/pelanggan',
        icon: <DashboardIcon />,
        items: [
            {
                name: 'Daftar Pelanggan',
                path: 'daftar-pelanggan',
                icon: <DashboardIcon />,
                component: DaftarPelanggan,
                public: true,
                actions: [
                    {
                        name: 'Profile',
                        path: ':id/profile',
                        component: DaftarPelangganProfile,
                        public: true,
                    },
                ],
            },
        ],
    },
];

export default routeMainMenu;

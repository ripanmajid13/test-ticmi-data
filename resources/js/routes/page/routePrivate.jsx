import React from 'react';

// icon
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import DashboardIcon from '@mui/icons-material/Dashboard';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import GroupIcon from '@mui/icons-material/Group';
import SettingsApplicationsIcon from '@mui/icons-material/SettingsApplications';
import SubtitlesIcon from '@mui/icons-material/Subtitles';
import TypeSpecimenIcon from '@mui/icons-material/TypeSpecimen';

import { MODULE_BACKEND } from '@configs/module';
import { MODULE_NAVIGATION_INFORMATION } from '@configs/moduleNavigation';

// page
import BackendApi from '@views/pages/backend/Api';
import BackendLog from '@views/pages/backend/Log';
import Example from '@views/pages/example';
import PrivilgesDocumentationComponent from '@views/pages/privileges/documentation/Component';
import PrivilgesDocumentationConfig from '@views/pages/privileges/documentation/Config';
import PrivilegesBackendLanguage from '@views/pages/privileges/Language';
import PrivilegesBackendLanguageForm from '@views/pages/privileges/Language/Form';
import PrivilegesBackendLanguageTrash from '@views/pages/privileges/Language/Trash';
import PrivilegesPermisson from '@views/pages/privileges/Permission';
import PrivilegesRole from '@views/pages/privileges/Role';
import PrivilegesRoleTrash from '@views/pages/privileges/Role/Trash';
import PrivilegesType from '@views/pages/privileges/Type';
import PrivilegesTypeTrash from '@views/pages/privileges/Type/Trash';
import PrivilegesUser from '@views/pages/privileges/User';
import PrivilegesUserForm from '@views/pages/privileges/User/Form';
import PrivilegesUserShow from '@views/pages/privileges/User/Show';
import PrivilegesUserTrash from '@views/pages/privileges/User/Trash';

const routePrivate = [
    {
        name: 'Privileges',
        path: '/privileges',
        icon: <DashboardIcon />,
        items: [
            {
                name: 'Type',
                path: 'type',
                icon: <TypeSpecimenIcon />,
                component: PrivilegesType,
                actions: [
                    {
                        name: 'Trash',
                        path: 'trash',
                        component: PrivilegesTypeTrash,
                    },
                ],
            },
            {
                name: 'User',
                path: 'user',
                icon: <GroupIcon />,
                component: PrivilegesUser,
                module: [MODULE_BACKEND],
                navigation: MODULE_NAVIGATION_INFORMATION,
                actions: [
                    {
                        name: 'Create',
                        path: 'create',
                        component: PrivilegesUserForm,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Show',
                        path: ':id/show',
                        component: PrivilegesUserShow,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Edit',
                        path: ':id/edit',
                        component: PrivilegesUserForm,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Trash',
                        path: 'trash',
                        component: PrivilegesUserTrash,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                ],
            },
            {
                name: 'Role',
                path: 'role',
                icon: <AccessibilityIcon />,
                component: PrivilegesRole,
                actions: [
                    {
                        name: 'Trash',
                        path: 'trash',
                        component: PrivilegesRoleTrash,
                    },
                ],
            },
            {
                name: 'Permission',
                path: 'permission',
                icon: <SettingsApplicationsIcon />,
                component: PrivilegesPermisson,
            },
            {
                name: 'Language',
                path: 'language',
                icon: <SubtitlesIcon />,
                component: PrivilegesBackendLanguage,
                actions: [
                    {
                        name: 'Create',
                        path: 'create',
                        component: PrivilegesBackendLanguageForm,
                    },
                    {
                        name: 'Edit',
                        path: ':id/edit',
                        component: PrivilegesBackendLanguageForm,
                    },
                    {
                        name: 'Trash',
                        path: 'trash',
                        component: PrivilegesBackendLanguageTrash,
                    },
                ],
            },
            {
                name: 'Dokumentation',
                path: 'dokumentation',
                icon: <DashboardIcon />,
                items: [
                    {
                        name: 'Component',
                        path: 'component',
                        icon: <DashboardIcon />,
                        component: PrivilgesDocumentationComponent,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Config',
                        path: 'config',
                        icon: <DashboardIcon />,
                        component: PrivilgesDocumentationConfig,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Helper',
                        path: 'helper',
                        icon: <DashboardIcon />,
                        component: Example,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                        actions: [
                            {
                                name: 'Create',
                                path: 'Create',
                                component: Example,
                                module: [MODULE_BACKEND],
                                navigation: MODULE_NAVIGATION_INFORMATION,
                            },
                        ],
                    },
                    {
                        name: 'Hook',
                        path: 'hook',
                        icon: <DashboardIcon />,
                        component: Example,
                    },
                    {
                        name: 'Service',
                        path: 'service',
                        icon: <DashboardIcon />,
                        component: Example,
                    },
                ],
            },
            {
                name: 'Dokumentation 1',
                path: 'dokumentation-1',
                icon: <DashboardIcon />,
                items: [
                    {
                        name: 'Component',
                        path: 'component',
                        icon: <DashboardIcon />,
                        component: PrivilgesDocumentationComponent,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Config',
                        path: 'config',
                        icon: <DashboardIcon />,
                        component: PrivilgesDocumentationConfig,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Helper',
                        path: 'helper',
                        icon: <DashboardIcon />,
                        component: Example,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                        actions: [
                            {
                                name: 'Create',
                                path: 'Create',
                                component: Example,
                                module: [MODULE_BACKEND],
                                navigation: MODULE_NAVIGATION_INFORMATION,
                            },
                        ],
                    },
                    {
                        name: 'Hook',
                        path: 'hook',
                        icon: <DashboardIcon />,
                        component: Example,
                    },
                    {
                        name: 'Service',
                        path: 'service',
                        icon: <DashboardIcon />,
                        component: Example,
                    },
                ],
            },
            {
                name: 'Dokumentation 2',
                path: 'dokumentation-2',
                icon: <DashboardIcon />,
                items: [
                    {
                        name: 'Component',
                        path: 'component',
                        icon: <DashboardIcon />,
                        component: PrivilgesDocumentationComponent,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Config',
                        path: 'config',
                        icon: <DashboardIcon />,
                        component: PrivilgesDocumentationConfig,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                    {
                        name: 'Helper',
                        path: 'helper',
                        icon: <DashboardIcon />,
                        component: Example,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                        actions: [
                            {
                                name: 'Create',
                                path: 'Create',
                                component: Example,
                                module: [MODULE_BACKEND],
                                navigation: MODULE_NAVIGATION_INFORMATION,
                            },
                        ],
                    },
                    {
                        name: 'Hook',
                        path: 'hook',
                        icon: <DashboardIcon />,
                        component: Example,
                    },
                    {
                        name: 'Service',
                        path: 'service',
                        icon: <DashboardIcon />,
                        component: Example,
                    },
                ],
            },
        ],
    },
    {
        name: 'Backend',
        path: '/backend',
        icon: <DashboardIcon />,
        items: [
            {
                name: 'Api',
                path: 'api',
                icon: <FormatListNumberedIcon />,
                component: BackendApi,
                module: [MODULE_BACKEND],
                navigation: MODULE_NAVIGATION_INFORMATION,
                actions: [
                    {
                        name: 'Create',
                        path: 'create',
                        component: BackendApi,
                        module: [MODULE_BACKEND],
                        navigation: MODULE_NAVIGATION_INFORMATION,
                    },
                ],
            },
            {
                name: 'Log',
                path: 'log',
                icon: <FormatListBulletedIcon />,
                component: BackendLog,
                module: [MODULE_BACKEND],
                navigation: MODULE_NAVIGATION_INFORMATION,
            },
        ],
    },
];

export default routePrivate;

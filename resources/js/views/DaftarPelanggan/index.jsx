import React, { useEffect, useState } from 'react';

import TableService from '@services/TableService';

import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';

import column from './column';
import Form from './Form';

const DaftarPelanggan = () => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialog, setDialog] = useState({});
    const [service, setService] = useState({
        uri: 'pelanggan',
        column,
        selection: true,
        actions: {
            store: {
                display: 'Create Pelanggan',
                dialog: true,
                dialogWidth: 'md',
                dialogRepeat: true,
                fn: (dataTable) => setDialog(dataTable),
            },
            profile: {
                uri: '/pelanggan-daftar-pelanggan',
                data: true,
                params: ['id'],
                display: 'Profile',
            },
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <Form {...dialog} setDialog={setDialog} />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

export default DaftarPelanggan;

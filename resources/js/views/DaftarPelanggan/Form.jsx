import React, { forwardRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import { Close as CloseIcon, Save as SaveIcon } from '@mui/icons-material';
import {
    Box,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    Grow,
} from '@mui/material';
import { CCol, CContainer, CFormInput, CFormLabel, CRow } from '@coreui/react';
import { MuiButton } from '@components/materialui/inputs';

import api from '@configs/api';
import apiCatch from '@configs/apiCatch';
import inputCreate from '@configs/inputCreate';
import inputEdit from '@configs/inputEdit';
import configMethod from '@configs/method';
import notistack from '@configs/notistack';
import xsrfToken from '@configs/xsrfToken';

import { RenderMap } from '@helpers/render';
import { formValidation } from '@hooks/form';
import { typeIsFunction, typeIsObject } from '@hooks/type';

const Transition = forwardRef(function Transition(props, ref) {
    const customProps = {
        ...props,
        timeout: {
            enter: 425,
            exit: 395,
        },
    };

    return <Grow ref={ref} {...customProps} />;
});

const Form = ({
    open,
    width,
    title,
    setDialog,
    method,
    type,
    state,
    pathname,
    payload,
    repeat,
    reloadRow,
}) => {
    const [submit, setSubmit] = useState(false);
    const [checked, setChecked] = useState(false);
    const [formData, setFormData] = useState([
        {
            id: 'nama_pelanggan',
            value: '',
            type: 'text',
            label: 'Nama Lengkap',
            placeholder: 'Nama Lengkap',
        },
        {
            id: 'username',
            value: '',
            type: 'text',
            label: 'Username',
            placeholder: 'Username',
        },
        {
            id: 'email',
            value: '',
            type: 'text',
            label: 'Email',
            placeholder: 'Email',
        },
        {
            id: 'no_hp',
            value: '',
            type: 'text',
            label: 'No HP',
            placeholder: 'No HP',
        },
    ]);

    useEffect(() => {
        if (open === true) {
            if (type === 'edit') {
                // eslint-disable-next-line prettier/prettier
                // setInput((prevState) => inputEdit(prevState, { data: typeIsObject(state) ? state : {} }));
            } else {
                // setInput((prevState) => inputCreate(prevState, {}));
            }
        }
    }, [open]);

    const handleOnSubmit = (e) => {
        e.preventDefault();

        setSubmit(true);
        const postData = async () => {
            try {
                const payload = new FormData();

                payload.append('_method', 'POST');
                formData.map((val) => {
                    payload.append(val.id, val.value);
                });

                await api
                    .post(pathname, payload)
                    .then(({ data, statusText }) => {
                        setSubmit(false);
                        notistack['success'](i18next.t(data.message || statusText));
                        if (reloadRow && typeIsFunction(reloadRow)) reloadRow();
                        setDialog((prevState) => ({
                            ...prevState,
                            open: checked,
                        }));
                    })
                    .catch(() => {
                        setSubmit(false);
                    });
            } catch (error) {
                setSubmit(false);
            }
        };
        setTimeout(() => {
            postData();
        }, 1000);
    };

    return (
        <Dialog
            fullWidth={true}
            open={open}
            maxWidth={width}
            transitionDuration={{ enter: 425, exit: 395 }}
            TransitionComponent={Transition}
            onClose={() =>
                !submit &&
                setDialog((prevState) => ({
                    ...prevState,
                    open: false,
                }))
            }
            keepMounted
            aria-describedby="alert-dialog-slide-description">
            <Box component="form" onSubmit={(e) => handleOnSubmit(e)} autoComplete="off" noValidate>
                <DialogTitle
                    sx={{
                        p: 1,
                        fontSize: '1.15rem',
                        textAlign: 'center',
                        borderBottom: 'solid 1px rgba(224, 224, 224, 1)',
                    }}
                    children={i18next.t(title)}
                />

                <DialogContent sx={{ py: '12px !important', px: '1px !important' }}>
                    <CContainer className="mw-100">
                        <CRow>
                            <CCol sm={7}>
                                <RenderMap
                                    data={formData}
                                    render={(val, key) => {
                                        switch (val.type) {
                                            case 'value':
                                                break;

                                            default:
                                                return (
                                                    <CRow
                                                        key={key}
                                                        className="mb-3 align-items-center">
                                                        <CFormLabel
                                                            htmlFor={val.name}
                                                            className="col-sm-4 col-form-label py-0"
                                                            children={val.label}
                                                        />
                                                        <CCol sm={8}>
                                                            <CFormInput
                                                                type="text"
                                                                size="sm"
                                                                id={val.name}
                                                                onChange={(e) => {
                                                                    setFormData((prevState) => {
                                                                        const newState =
                                                                            prevState.map((obj) => {
                                                                                // eslint-disable-next-line prettier/prettier
                                                                                if (obj.id === val.id) {
                                                                                    return {
                                                                                        ...obj,
                                                                                        // eslint-disable-next-line prettier/prettier
                                                                                        value: e.target.value,
                                                                                    };
                                                                                }
                                                                                return obj;
                                                                            });

                                                                        return newState;
                                                                    });
                                                                }}
                                                            />
                                                        </CCol>
                                                    </CRow>
                                                );
                                        }
                                    }}
                                />
                            </CCol>

                            <CCol sm={5}>{/* <pre>{JSON.stringify(input, null, 2)}</pre> */}</CCol>
                        </CRow>
                    </CContainer>
                </DialogContent>

                <DialogActions
                    sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        borderTop: 'solid 1px rgba(224, 224, 224, 1)',
                    }}>
                    <Box>
                        <MuiButton
                            variant="outlined"
                            color="error"
                            onClick={() =>
                                !submit &&
                                setDialog((prevState) => ({
                                    ...prevState,
                                    open: false,
                                }))
                            }
                            children="Cancel"
                            fullWidth={false}
                            startIcon={<CloseIcon />}
                        />
                        &nbsp;
                        <MuiButton
                            loadingButton={true}
                            loadingProcess={submit}
                            type="submit"
                            color="success"
                            children="Save"
                            fullWidth={false}
                            startIcon={<SaveIcon />}
                        />
                    </Box>
                </DialogActions>
            </Box>
        </Dialog>
    );
};

Form.defaultProps = {
    open: false,
    width: 'xs',
    title: '',
    method: configMethod[0],
    type: 'create',
    state: {},
    repeat: false,
    payload: {},
};

Form.propTypes = {
    open: PropTypes.bool,
    width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
    title: PropTypes.string,
    setDialog: PropTypes.func.isRequired,
    method: PropTypes.oneOf(configMethod).isRequired,
    type: PropTypes.oneOf(['create', 'edit']).isRequired,
    state: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    pathname: PropTypes.string,
    payload: PropTypes.object,
    repeat: PropTypes.bool,
    reloadRow: PropTypes.func,
};

export default Form;

import { stringToDateTime } from '@hooks/string';

const column = [
    {
        Header: 'Tanggal Bergabung',
        accessor: 'created_at',
        Cell: ({ cell: { value } }) => stringToDateTime(value, 'd/m/Y'),
        filter: true,
        filterType: 'daterange',
    },
    {
        Header: 'Pelanggan',
        accessor: 'nama_pelanggan',
        filter: true,
    },
    {
        Header: 'Tipe Pelanggan',
        accessor: 'name',
        filter: true,
    },
    {
        Header: 'Status Berlangganan',
        accessor: 'status_pelanggan',
        filter: true,
    },
    {
        Header: 'Status Akun',
        accessor: 'status_akun',
        filter: true,
    },
];

export default column;

import React from 'react';
import { useLocation } from 'react-router-dom';

import { Paper } from '@mui/material';

const Profile = () => {
    const location = useLocation();
    return (
        <Paper
            elevation={0}
            // eslint-disable-next-line prettier/prettier
            sx={{ py: 1, px: 0, boxShadow: 'rgb(51 48 60 / 3%) 0px 2px 7px 1px, rgb(51 48 60 / 2%) 0px 4px 7px 0px, rgb(51 48 60 / 1%) 0px 1px 4px 2px' }}>
            <h1>Nama Pelanggan : {location.state?.nama_pelanggan}</h1>
        </Paper>
    );
};

export default Profile;

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import TableService from '@services/TableService';

import { DialogAuth } from '@helpers/dialog';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';

import column from './column';

const Api = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [update, setUpdate] = useState({});
    const [service, setService] = useState({
        uri,
        actions: {
            update: {
                title: 'Update Api',
                dialog: true,
                dialogEdit: true,
                fn: (dataTable) => setUpdate(dataTable),
            },
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogAuth {...update} setDialog={setUpdate} />
            <TableData {...service} column={column} children={data} setService={setService} />
        </LoadingTable>
    );
};

Api.propTypes = {
    uri: PropTypes.string,
};

export default Api;

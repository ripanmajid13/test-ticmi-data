import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import TableService from '@services/TableService';

import { DialogConfirmation } from '@helpers/dialog';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';
import { typeIsUndefined } from '@hooks/type';

import column from './column';

const Api = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialog, setDialog] = useState({});
    const [service, setService] = useState({
        uri,
        column,
        selection: true,
        sx: ({ original }) =>
            (original.middleware < 1 || typeIsUndefined(original.middleware)) && {
                backgroundColor: 'error.light',
            },
        actions: {
            destroy: {
                data: true,
                params: ['id'],
                display: 'Delete',
                dialog: true,
                dialogText: 'Are you sure you want to delete this data',
                disabled: ({ middleware }) => middleware > 0 || typeIsUndefined(middleware),
                fn: (dataTable) => setDialog(dataTable),
            },
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogConfirmation {...dialog} setDialog={setDialog} />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

Api.propTypes = {
    uri: PropTypes.string,
};

export default Api;

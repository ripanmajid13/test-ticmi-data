import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import { Divider, List, ListItem, ListItemText } from '@mui/material';

import TableService from '@services/TableService';

import { DialogAuth, DialogDownload, DialogPrint, DialogWrapper } from '@helpers/dialog';
import { FormDialogLoading } from '@helpers/form';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';
import { typeIsUndefined } from '@hooks/type';

import column from './column';

const Api = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialogData, setDialogData] = useState({});
    const [dialogDetail, setDialogDetail] = useState({});
    const [dialogDelete, setDialogDelete] = useState({});
    const [dialogDownload, setDialogDownload] = useState({});
    const [dialogPrint, setDialogPrint] = useState({});
    const [formData, setFormData] = useState({
        type: {
            value: '',
            type: 'select',
            label: 'Type',
            placeholder: 'Type',
            required: true,
            gridSm: 6,
        },
        display: {
            value: '',
            type: 'text',
            label: 'Display',
            placeholder: 'Display',
            required: true,
            gridSm: 6,
        },
        permissions: {
            value: '',
            valueEdit: 'permissions_id',
            type: 'select',
            multiple: true,
            label: 'Permissions',
            placeholder: 'Permissions',
            gridSm: 12,
        },
        users: {
            value: '',
            valueEdit: 'users_id',
            type: 'select',
            multiple: true,
            searchable: true,
            label: 'Users',
            placeholder: 'Users',
            gridSm: 12,
        },
    });
    const [service, setService] = useState({
        uri,
        column,
        selection: true,
        actions: {
            create: {
                display: 'Add',
                dialog: true,
                dialogWidth: 'sm',
                dialogRepeat: true,
                dialogLoading: true,
                dialogLoadingAction: 'store',
                fn: (dataTable) => setDialogData(dataTable),
            },
            show: {
                data: true,
                params: ['id'],
                display: 'View',
                dialog: true,
                dialogEdit: true,
                dialogWidth: 'md',
                fn: (dataTable) => setDialogDetail(dataTable),
            },
            edit: {
                data: true,
                params: ['id'],
                display: 'Edit',
                dialog: true,
                dialogEdit: true,
                dialogWidth: 'sm',
                dialogLoading: true,
                dialogLoadingAction: 'update',
                fn: (dataTable) => setDialogData(dataTable),
            },
            destroy: {
                data: true,
                params: ['id'],
                display: 'Delete',
                dialog: true,
                dialogText: 'Are you sure you want to delete this data',
                dialogDestroy: true,
                disabled: ({ users, permissions }) =>
                    typeIsUndefined(users) || typeIsUndefined(permissions)
                        ? true
                        : users || permissions
                        ? true
                        : false,
                fn: (dataTable) => setDialogDelete(dataTable),
            },
            download_excel: {
                display: 'Excel',
                dialog: true,
                dialogTitle: 'Download Excel',
                dialogDownload: true,
                dialogDownloadFile: 'Role',
                fn: (dataTable) => setDialogDownload(dataTable),
            },
            download_pdf: {
                display: 'PDF',
                dialog: true,
                dialogTitle: 'Download PDF',
                dialogDownload: true,
                dialogDownloadFile: 'Role',
                fn: (dataTable) => setDialogDownload(dataTable),
            },
            print: {
                display: 'Print',
                dialog: true,
                dialogTitle: 'Print PDF',
                dialogPrint: true,
                dialogPrintFile: 'Role',
                fn: (dataTable) => setDialogPrint(dataTable),
            },
            trash: true,
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogAuth {...dialogDelete} setDialog={setDialogDelete} />
            <DialogDownload {...dialogDownload} setDialog={setDialogDownload} />
            <DialogPrint {...dialogPrint} setDialog={setDialogPrint} />
            <DialogWrapper {...dialogDetail} setDialog={setDialogDetail}>
                <List dense>
                    <ListItem>
                        <ListItemText
                            primary={i18next.t('Code') + ' :'}
                            secondary={dialogDetail.state?.code}
                            sx={{ my: 0, '& .MuiListItemText-primary': { fontWeight: 'bold' } }}
                        />

                        <ListItemText
                            primary={i18next.t('Name') + ' :'}
                            secondary={dialogDetail.state?.name}
                            sx={{ my: 0, '& .MuiListItemText-primary': { fontWeight: 'bold' } }}
                        />

                        <ListItemText
                            primary={i18next.t('Type') + ' :'}
                            secondary={dialogDetail.state?.type_name}
                            sx={{ my: 0, '& .MuiListItemText-primary': { fontWeight: 'bold' } }}
                        />

                        <ListItemText
                            primary={i18next.t('Display') + ' :'}
                            secondary={dialogDetail.state?.display}
                            sx={{ my: 0, '& .MuiListItemText-primary': { fontWeight: 'bold' } }}
                        />
                    </ListItem>
                </List>

                <Divider />

                <List dense>
                    <ListItem>
                        <ListItemText
                            primary={i18next.t('Permissions') + ' :'}
                            secondary={
                                dialogDetail.state?.permissions
                                    ? JSON.parse(dialogDetail.state?.permissions)
                                          .map((val, key) => `${key + 1}. ${val}`)
                                          .join(', ')
                                    : '-'
                            }
                            sx={{ my: 0, '& .MuiListItemText-primary': { fontWeight: 'bold' } }}
                        />
                    </ListItem>
                </List>

                <Divider />

                <List dense>
                    <ListItem>
                        <ListItemText
                            primary={i18next.t('Users') + ' :'}
                            secondary={
                                dialogDetail.state?.users
                                    ? JSON.parse(dialogDetail.state?.users)
                                          .map((val, key) => `${key + 1}. ${val}`)
                                          .join(', ')
                                    : '-'
                            }
                            sx={{ my: 0, '& .MuiListItemText-primary': { fontWeight: 'bold' } }}
                        />
                    </ListItem>
                </List>
            </DialogWrapper>
            <FormDialogLoading
                {...dialogData}
                setDialog={setDialogData}
                input={formData}
                setInput={setFormData}
            />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

Api.propTypes = {
    uri: PropTypes.string,
};

export default Api;

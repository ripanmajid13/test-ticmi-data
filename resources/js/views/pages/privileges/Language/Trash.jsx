import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import TableService from '@services/TableService';

import { DialogConfirmation } from '@helpers/dialog';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';

import column from './column';

const Trash = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialog, setDialog] = useState({});
    const [service, setService] = useState({
        uri,
        column,
        back: uri.replace('/trash', ''),
        multiple: true,
        selection: true,
        actions: {
            restore: {
                uri: uri.replace('/trash', ''),
                data: true,
                dataMultiple: true,
                display: 'Restore',
                dialog: true,
                dialogEdit: true,
                dialogTitle: 'Restore Data',
                dialogText: 'Are you sure you want to restore this data',
                dialogDataRow: true,
                fn: (dataTable) => setDialog(dataTable),
            },
            destroy_permanent: {
                uri: uri.replace('/trash', ''),
                data: true,
                dataMultiple: true,
                display: 'Permanent Delete',
                dialog: true,
                dialogWidth: 'sm',
                dialogDestroy: true,
                dialogTitle: 'Permanent Delete Data',
                dialogText: 'Are you sure you want to delete this data permanently',
                dialogDataRow: true,
                fn: (dataTable) => setDialog(dataTable),
            },
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogConfirmation {...dialog} setDialog={setDialog} />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

Trash.propTypes = {
    uri: PropTypes.string,
};

export default Trash;

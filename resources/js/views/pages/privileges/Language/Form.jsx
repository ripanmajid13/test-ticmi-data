import React, { memo, useState } from 'react';
import { useParams } from 'react-router-dom';

import { FormPage } from '@helpers/form';

const Form = () => {
    const params = useParams();
    const [formInput, setFormInput] = useState({
        key: {
            value: '',
            type: 'text',
            label: 'Key',
            placeholder: 'Key',
            required: true,
        },
        lang_en: {
            value: '',
            type: 'textarea',
            label: 'EN',
            placeholder: 'EN',
        },
        lang_id: {
            value: '',
            type: 'textarea',
            label: 'ID',
            placeholder: 'ID',
        },
    });

    return (
        <FormPage
            home="/privileges-language"
            input={formInput}
            setInput={setFormInput}
            {...(params.id
                ? {
                      type: 'edit',
                      method: 'PUT',
                      action: `/${params.id}/edit`,
                      repeat: false,
                  }
                : {
                      type: 'create',
                      method: 'POST',
                      action: '/create',
                      repeat: true,
                  })}
        />
    );
};

export default memo(Form);

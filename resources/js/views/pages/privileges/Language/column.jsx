const column = [
    {
        Header: 'Key',
        accessor: 'key',
        filter: true,
    },
    {
        Header: 'EN',
        accessor: 'lang_en',
    },
    {
        Header: 'ID',
        accessor: 'lang_id',
    },
    {
        Header: 'Updated',
        accessor: 'updated',
    },
];

export default column;

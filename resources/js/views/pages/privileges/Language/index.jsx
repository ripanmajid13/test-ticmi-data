import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import TableService from '@services/TableService';

import { DialogAuth } from '@helpers/dialog';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';

import column from './column';

const Language = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialog, setDialog] = useState({});
    const [service, setService] = useState({
        uri,
        column,
        selection: true,
        actions: {
            helper: {
                dialog: true,
                dialogTitle: 'Helper',
                fn: (dataTable) => setDialog(dataTable),
            },
            create: {
                display: 'Add',
            },
            edit: {
                data: true,
                params: ['id'],
                // params: ['id', { status: 'true' }],
                display: 'Edit',
            },
            destroy: {
                data: true,
                params: ['id'],
                display: 'Delete',
                dialog: true,
                dialogText: 'Are you sure you want to delete this data',
                dialogDestroy: true,
                fn: (dataTable) => setDialog(dataTable),
            },
            trash: true,
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogAuth {...dialog} setDialog={setDialog} />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

Language.propTypes = {
    uri: PropTypes.string,
};

export default Language;

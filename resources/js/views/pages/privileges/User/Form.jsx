import React, { memo, useState } from 'react';
import { useParams } from 'react-router-dom';

import { FormPageLoading } from '@helpers/form';

const Form = () => {
    let params = useParams();

    const [formInput, setFormInput] = useState({
        first_name: {
            value: '',
            type: 'text',
            label: 'First Name',
            placeholder: 'First Name',
            required: true,
            gridSm: 4,
        },
        last_name: {
            value: '',
            type: 'text',
            label: 'Last Name',
            placeholder: 'Last Name',
            required: true,
            gridSm: 4,
        },
        date_of_birth: {
            value: '',
            type: 'date',
            label: 'Date Of Birth',
            placeholder: 'Date Of Birth',
            required: true,
            gridSm: 2,
        },
        active: {
            value: '',
            valueDefault: 'true',
            type: 'select',
            label: 'Active',
            placeholder: 'Active',
            required: true,
            gridSm: 2,
        },
        email: {
            value: '',
            type: 'text',
            valueType: 'email',
            label: 'Email',
            placeholder: 'Email',
            required: true,
        },
        username: {
            value: '',
            type: 'text',
            label: 'Username',
            placeholder: 'Username',
            required: true,
        },
        password: {
            value: '',
            type: 'text',
            valueEncrypt: true,
            label: 'Password',
            placeholder: 'Password',
            required: true,
            ...(params.id && {
                note: 'Ignore it if it will not be changed.',
            }),
        },
        permissions: {
            value: '',
            valueEdit: 'permissions_id',
            type: 'select',
            multiple: true,
            label: 'Permissions',
            placeholder: 'Permissions',
            gridSm: 12,
        },
    });

    return (
        <FormPageLoading
            home="/privileges-user"
            input={formInput}
            setInput={setFormInput}
            {...(params.id
                ? {
                      type: 'edit',
                      method: 'PUT',
                      action: `/${params.id}/update`,
                      repeat: false,
                  }
                : {
                      type: 'create',
                      method: 'POST',
                      action: '/store',
                      repeat: true,
                  })}
        />
    );
};

export default memo(Form);

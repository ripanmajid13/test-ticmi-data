import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import TableService from '@services/TableService';

import { DialogAuth } from '@helpers/dialog';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';
import { typeIsUndefined } from '@hooks/type';

import column from './column';

const Api = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialog, setDialog] = useState({});
    const [service, setService] = useState({
        uri,
        column,
        selection: true,
        sx: ({ original }) =>
            original.active !== 'true' && {
                color: 'white !important',
                backgroundColor: 'error.light',
            },
        actions: {
            create: {
                display: 'Add',
            },
            show: {
                data: true,
                params: ['id'],
                display: 'View',
            },
            edit: {
                data: true,
                params: ['id'],
                display: 'Edit',
            },
            destroy: {
                data: true,
                params: ['id'],
                display: 'Delete',
                // eslint-disable-next-line prettier/prettier
                disabled: ({ roles, permissions }) => ((typeIsUndefined(roles) || typeIsUndefined(permissions)) ? true : roles ? true : false),
                dialog: true,
                dialogText: 'Are you sure you want to delete this data',
                dialogDestroy: true,
                fn: (dataTable) => setDialog(dataTable),
            },
            trash: true,
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes, column });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogAuth {...dialog} setDialog={setDialog} />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

Api.propTypes = {
    uri: PropTypes.string,
};

export default Api;

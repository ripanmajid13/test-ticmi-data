const column = [
    {
        Header: 'Name',
        accessor: 'name',
        filter: true,
    },
    {
        Header: 'Roles',
        accessor: 'roles',
        sorted: false,
        Cell: ({ cell: { value } }) => {
            if (value !== null && value !== undefined) {
                let result = [];

                JSON.parse(value).map((vm) => result.push(vm.label));

                return result.join(', ');
            } else {
                return '-';
            }
        },
    },
];

export default column;

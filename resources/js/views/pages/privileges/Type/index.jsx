import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import TableService from '@services/TableService';

import { DialogAuth } from '@helpers/dialog';
import { FormDialog } from '@helpers/form';
import { LoadingTable } from '@helpers/loading';
import { TableData } from '@helpers/table';
import { typeIsUndefined } from '@hooks/type';

import column from './column';

const Api = ({ uri }) => {
    const [res, setRes] = useState({});
    const [data, setData] = useState({});
    const [dialog, setDialog] = useState({});
    const [dialogData, setDialogData] = useState({});
    const [formData, setFormData] = useState({
        name: {
            value: '',
            type: 'text',
            label: 'Name',
            placeholder: 'Name',
            required: true,
            gridSm: 12,
        },
    });
    const [service, setService] = useState({
        uri,
        column,
        selection: true,
        actions: {
            store: {
                display: 'Add',
                dialog: true,
                dialogRepeat: true,
                fn: (dataTable) => setDialogData(dataTable),
            },
            update: {
                data: true,
                params: ['id'],
                display: 'Edit',
                dialog: true,
                dialogEdit: true,
                fn: (dataTable) => setDialogData(dataTable),
            },
            destroy: {
                data: true,
                params: ['id'],
                display: 'Delete',
                dialog: true,
                dialogText: 'Are you sure you want to delete this data',
                dialogDestroy: true,
                disabled: ({ roles }) => (typeIsUndefined(roles) ? true : roles ? true : false),
                fn: (dataTable) => setDialog(dataTable),
            },
            trash: true,
        },
    });

    useEffect(() => {
        let mounted = true;
        TableService({ mounted, service, setRes });
        return () => (mounted = false);
    }, [service]);

    return (
        <LoadingTable result={res} setData={setData}>
            <DialogAuth {...dialog} setDialog={setDialog} />
            <FormDialog
                {...dialogData}
                setDialog={setDialogData}
                input={formData}
                setInput={setFormData}
            />
            <TableData {...service} setService={setService} children={data} />
        </LoadingTable>
    );
};

Api.propTypes = {
    uri: PropTypes.string,
};

export default Api;

/* eslint-disable no-useless-escape */
import i18next from 'i18next';

import apiCatch from '@configs/apiCatch';
import { encrypt } from '@configs/crypto';
import notistack from '@configs/notistack';

import { typeIsFunction, typeIsObject } from '@hooks/type';

const formValidation = (data, setData) => {
    return new Promise((resolve) => {
        if (!typeIsObject(data)) {
            notistack['error'](i18next.t('data must be object.'));
        } else if (!typeIsFunction(setData)) {
            notistack['error'](i18next.t('setData must be function.'));
        } else {
            let error = {};
            let success = {};

            for (const [key, val] of Object.entries(data)) {
                let valError = [];

                // required
                if (
                    val.required === true &&
                    (val.value === undefined || val.value === '' || val.value === null)
                ) {
                    valError.push({
                        [key]: [i18next.t(`The ${val.label.toLowerCase()} field is required.`)],
                    });
                }

                // email
                if (val.valueType === 'email') {
                    const checkEmail = val.value.match(
                        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    );

                    if (!checkEmail) {
                        valError.push({
                            [key]: [i18next.t(`The ${val.label.toLowerCase()} field is email.`)],
                        });
                    }
                }

                // ---------------------------------------------

                if (valError.length < 1) {
                    // password
                    if (val.valueEncrypt === true) {
                        success[key] = encrypt(val.value);
                    } else {
                        success[key] = val.value;
                    }
                } else {
                    error[key] = Object.values(valError.shift());
                }
            }

            if (Object.keys(error).length < 1) {
                resolve(success);
            } else {
                let resReject = {};
                for (const [keyRejectError, valRejectError] of Object.entries(error)) {
                    resReject[keyRejectError] = valRejectError.shift();
                }
                apiCatch(
                    {
                        response: {
                            status: 422,
                            statusText: 'Unprocessable Content',
                            data: {
                                errors: resReject,
                                message: 'The given data was invalid.',
                            },
                        },
                    },
                    setData
                );
            }
        }
    });
};

export default formValidation;

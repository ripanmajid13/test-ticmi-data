import React, { useState } from 'react';
import PropTypes from 'prop-types';
import i18next from 'i18next';

import {
    Cached as CachedIcon,
    LockPerson as LockPersonIcon,
    Logout as LogoutIcon,
} from '@mui/icons-material';
import {
    Avatar,
    Box,
    Divider,
    IconButton,
    ListItemIcon,
    Menu,
    MenuItem,
    Typography,
} from '@mui/material';

import color from '@configs/color';
import { decrypt, encrypt } from '@configs/crypto';
import xsrfToken from '@configs/xsrfToken';

import { BackdropServicePost } from '@services/BackdropService';

import { STORE_TYPE_APP_FLUSH_CACHE, STORE_TYPE_APP_LOGOUT } from '@store/type/app';

const User = ({ dispatch, token }) => {
    const [anchorEl, setAnchorEl] = useState(null);

    const avatarUser = (name) => {
        return {
            sx: {
                width: 40,
                height: 40,
                bgcolor: () => {
                    let hash = 0;
                    let i;

                    /* eslint-disable no-bitwise */
                    for (i = 0; i < name.length; i += 1) {
                        hash = name.charCodeAt(i) + ((hash << 5) - hash);
                    }

                    let color = '#';

                    for (i = 0; i < 3; i += 1) {
                        const value = (hash >> (i * 8)) & 0xff;
                        color += `00${value.toString(16)}`.slice(-2);
                    }
                    /* eslint-enable no-bitwise */

                    return color;
                },
                fontSize: '20px',
                marginRight: 0,
            },
            children:
                name.split(' ').length < 2
                    ? `${name.split(' ')[0][0]}`
                    : `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`,
        };
    };

    return (
        <>
            <IconButton
                size="small"
                onClick={(event) => setAnchorEl(event.currentTarget)}
                sx={{ ml: 2 }}>
                <Avatar {...avatarUser(xsrfToken().name ?? '')} />
            </IconButton>

            <Menu
                anchorEl={anchorEl}
                id="user-menu"
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
                onClick={() => setAnchorEl(null)}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        mt: '7px',
                        ml: '-5px',
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        '& .MuiAvatar-root': {
                            width: 40,
                            height: 40,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&:before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 0,
                            right: 14,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                        '& .MuiList-root': {
                            p: 1,
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}>
                <MenuItem sx={{ px: 1, display: 'flex', alignItems: 'center' }} onClick={() => {}}>
                    <Avatar {...avatarUser(xsrfToken().name ?? '')} />

                    <Box>
                        <Typography
                            variant="body1"
                            gutterBottom
                            children={xsrfToken().name}
                            sx={{
                                margin: 0,
                                fontWeight: 'bold',
                                fontSize: '1.125rem',
                                fontFamily: `"Public Sans", sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"`,
                                color: color.text100,
                            }}
                        />

                        <Typography
                            variant="caption"
                            display="block"
                            gutterBottom
                            children={xsrfToken().email}
                            sx={{
                                margin: 0,
                                fontSize: '0.625rem',
                                fontFamily: `"Public Sans", sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"`,
                                color: color.text100,
                            }}
                        />
                    </Box>
                </MenuItem>

                <Divider sx={{ borderColor: color.text200 }} />

                <MenuItem
                    sx={{ px: 1 }}
                    onClick={() => {
                        BackdropServicePost({
                            uri: '/flush-cache',
                            title: 'Flush Cache',
                        }).then(({ data }) => {
                            for (let [key, val] of Object.entries(JSON.parse(decrypt(data)).lang)) {
                                i18next.addResourceBundle(key, 'translation', val.translation);
                            }
                            dispatch({
                                type: STORE_TYPE_APP_FLUSH_CACHE,
                                token: encrypt(
                                    JSON.stringify({
                                        ...JSON.parse(decrypt(token)),
                                        ...JSON.parse(decrypt(data)),
                                    })
                                ),
                            });
                        });
                    }}>
                    <ListItemIcon>
                        <CachedIcon fontSize="small" />
                    </ListItemIcon>
                    {i18next.t('Flush Cache')}
                </MenuItem>

                <MenuItem sx={{ px: 1 }} onClick={() => {}}>
                    <ListItemIcon>
                        <LockPersonIcon fontSize="small" />
                    </ListItemIcon>
                    {i18next.t('Lock Account')}
                </MenuItem>

                <MenuItem
                    sx={{ px: 1 }}
                    onClick={() =>
                        BackdropServicePost({
                            uri: '/logout',
                            title: 'Logout',
                        }).then(() =>
                            dispatch({
                                type: STORE_TYPE_APP_LOGOUT,
                            })
                        )
                    }>
                    <ListItemIcon>
                        <LogoutIcon fontSize="small" />
                    </ListItemIcon>
                    {i18next.t('Logout')}
                </MenuItem>
            </Menu>
        </>
    );
};

User.propTypes = {
    dispatch: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
};

export default User;

import React from 'react';
import { connect } from 'react-redux';

import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { Box, Breadcrumbs } from '@mui/material';

import routeBreadcrumb from './routeBreadcrumb';

const ContentBreadcrumb = () => {
    return (
        <Box
            sx={{
                mt: '.5rem',
                pr: 4,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
            }}>
            <Breadcrumbs
                sx={{
                    px: '1.5rem',
                    pt: 0,
                    display: 'block',
                    fontSize: '12px',
                    position: 'relative',
                    zIndex: 0,
                    '& .MuiBreadcrumbs-ol': {
                        marginLeft: '1px',
                        marginRight: '1px',
                    },
                }}
                separator={<NavigateNextIcon sx={{ fontSize: '14px' }} />}
                aria-label="breadcrumb"
                // eslint-disable-next-line prettier/prettier
                children={routeBreadcrumb()}
            />
        </Box>
    );
};

export default connect(null, null)(ContentBreadcrumb);

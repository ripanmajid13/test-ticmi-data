<?php

use App\Http\Controllers\PelangganController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(PelangganController::class)->name('pelanggan.')->prefix('pelanggan')
->group(function () {
    Route::post('', 'index');
    Route::post('/store', 'store');
    Route::put('/update', 'update');
});


// Route::post('/login', LoginController::class);
// Route::post('/register', RegisterController::class);

// Route::middleware('auth:sanctum')->group(function () {
//     Route::post('/flush-cache', FlushCacheController::class);
//     Route::post('/lock-account', LockAccountController::class);
//     Route::post('/logout', LogoutController::class);

//     // --------------- backend ---------------

//     Route::controller(ApiController::class)->name('backend-api.')->prefix('backend-api')
//     ->group(function () {
//         Route::post('', 'index');
//         Route::put('/update', 'update');
//     });

//     // --------------- database ---------------

//     if (Schema::hasTable('backend_apis')) {
//         $routes = Api::orderBy('method_id', 'asc')->orderBy('uri', 'ASC')->get();
//         foreach ($routes as $route) {
//             if (file_exists(app_path(Str::replace('\\', '/', Str::replace('\\App\\', '', $route->controller)).'.php'))) {
//                 if ($route->middleware == true) {
//                     if ($route->action == '__invoke') {
//                         Route::match(json_decode($route->methods), $route->uri, $route->controller)->middleware(['permission:'.$route->permission]);
//                     } else {
//                         Route::match(json_decode($route->methods), $route->uri, [$route->controller, $route->action])->middleware(['permission:'.$route->permission]);
//                     }
//                 } else {
//                     if ($route->action == '__invoke') {
//                         Route::match(json_decode($route->methods), $route->uri, $route->controller);
//                     } else {
//                         Route::match(json_decode($route->methods), $route->uri, [$route->controller, $route->action]);
//                     }
//                 }
//             }
//         }
//     }
// });

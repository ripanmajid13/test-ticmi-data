<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function index(Request $request)
    {
        return HelperDataTableDefault(new Pelanggan(), $request, [], [], [
            // ['name', 'asc'],
        ]);
    }

    public function store(Request $request)
    {
        $data  = $request->except('_method');
        $model = new Pelanggan();
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        $model->save();

        return response()->json([
            'message' => HelperMessageSuccessStore(),
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

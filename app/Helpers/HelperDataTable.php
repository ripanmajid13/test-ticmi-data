<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

if (!function_exists('HelperDataTableDefault')) {
    function HelperDataTableDefault($model, $request, $select = [], $where = [], $order = [], $filter = []) {
        $tables = $model::when(collect(json_decode($request->datatable_filter))->count() > 0, function ($query) use($request) {
            collect(json_decode($request->datatable_filter))->each(function ($item) use($query) {
                if ($item->type == 'text' || $item->type == 'select' || $item->type == 'date') {
                    $query->whereRaw('LOWER("'.$item->key.'") LIKE ? ', ['%'.Str::lower($item->value).'%']);
                } else if ($item->type == 'daterange') {
                    $query->whereBetween($item->key, Str::of($item->value)->explode(' - '));
                }
            });
        })->when(($request->datatable_sorting && count(json_decode($request->datatable_sorting)) > 0) || count($order), function ($query) use($request, $order) {
            if ($request->datatable_sorting && count(json_decode($request->datatable_sorting)) > 0) {
                $sorting = json_decode($request->datatable_sorting, true);
                if (count($sorting[0]) == 2) {
                    $query->orderBy(Arr::get($sorting[0], 'id'), Arr::get($sorting[0], 'desc') ? 'desc' : 'asc');
                }
            } else if (count($order)) {
                collect($order)->each(function ($item) use($query) {
                    if (count($item) == 2) {
                        $query->orderBy(Arr::get($item, 0, ''), Arr::get($item, 1, ''));
                    }
                });
            }
        })->paginate($request->paginate ?? 10);

        $result = collect($tables);

        $result->forget('first_page_url');
        $result->forget('last_page_url');
        $result->forget('links');
        $result->forget('next_page_url');
        $result->forget('path');
        $result->forget('per_page');
        $result->forget('prev_page_url');

        $result->put('data_filter', count($filter) < 1 ? json_decode('{}') : collect($filter));

        return $result->all();
    }
}

if (!function_exists('HelperDataTableOnlyTrashed')) {
    function HelperDataTableOnlyTrashed($model, $request, $select = [], $where = [], $order = [], $filter = []) {
        $tables = $model::onlyTrashed()->when(collect(json_decode($request->datatable_filter))->count() > 0, function ($query) use($request) {
            collect(json_decode($request->datatable_filter))->each(function ($item) use($query) {
                if ($item->type == 'text' || $item->type == 'select' || $item->type == 'date') {
                    $query->whereRaw('LOWER("'.$item->key.'") LIKE ? ', ['%'.Str::lower($item->value).'%']);
                } else if ($item->type == 'daterange') {
                    $query->whereBetween($item->key, Str::of($item->value)->explode(' - '));
                }
            });
        })->when(($request->datatable_sorting && count(json_decode($request->datatable_sorting)) > 0) || count($order), function ($query) use($request, $order) {
            if ($request->datatable_sorting && count(json_decode($request->datatable_sorting)) > 0) {
                $sorting = json_decode($request->datatable_sorting, true);
                if (count($sorting[0]) == 2) {
                    $query->orderBy(Arr::get($sorting[0], 'id'), Arr::get($sorting[0], 'desc') ? 'desc' : 'asc');
                }
            } else if (count($order)) {
                collect($order)->each(function ($item) use($query) {
                    if (count($item) == 2) {
                        $query->orderBy(Arr::get($item, 0, ''), Arr::get($item, 1, ''));
                    }
                });
            }
        })->paginate($request->paginate ?? 10);

        $result = collect($tables);

        $result->forget('first_page_url');
        $result->forget('last_page_url');
        $result->forget('links');
        $result->forget('next_page_url');
        $result->forget('path');
        $result->forget('per_page');
        $result->forget('prev_page_url');

        $result->put('data_filter', count($filter) < 1 ? json_decode('{}') : collect($filter));

        return $result->all();
    }
}

if (!function_exists('HelperDataTableWithTrashed')) {
    function HelperDataTableWithTrashed($model, $request, $select = [], $where = [], $order = [], $filter = []) {
        $tables = $model::withTrashed()->when(collect(json_decode($request->datatable_filter))->count() > 0, function ($query) use($request) {
            collect(json_decode($request->datatable_filter))->each(function ($item) use($query) {
                if ($item->type == 'text' || $item->type == 'select' || $item->type == 'date') {
                    $query->whereRaw('LOWER("'.$item->key.'") LIKE ? ', ['%'.Str::lower($item->value).'%']);
                } else if ($item->type == 'daterange') {
                    $query->whereBetween($item->key, Str::of($item->value)->explode(' - '));
                }
            });
        })->when(($request->datatable_sorting && count(json_decode($request->datatable_sorting)) > 0) || count($order), function ($query) use($request, $order) {
            if ($request->datatable_sorting && count(json_decode($request->datatable_sorting)) > 0) {
                $sorting = json_decode($request->datatable_sorting, true);
                if (count($sorting[0]) == 2) {
                    $query->orderBy(Arr::get($sorting[0], 'id'), Arr::get($sorting[0], 'desc') ? 'desc' : 'asc');
                }
            } else if (count($order)) {
                collect($order)->each(function ($item) use($query) {
                    if (count($item) == 2) {
                        $query->orderBy(Arr::get($item, 0, ''), Arr::get($item, 1, ''));
                    }
                });
            }
        })->paginate($request->paginate ?? 10);

        $result = collect($tables);

        $result->forget('first_page_url');
        $result->forget('last_page_url');
        $result->forget('links');
        $result->forget('next_page_url');
        $result->forget('path');
        $result->forget('per_page');
        $result->forget('prev_page_url');

        $result->put('data_filter', count($filter) < 1 ? json_decode('{}') : collect($filter));

        return $result->all();
    }
}
